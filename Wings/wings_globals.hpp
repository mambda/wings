#pragma once
#include "../../../generic_header.hpp"
#include "flyff_structs.hpp"
#include <chrono>
#include <vector>
#include <sstream>

// everything about the inner workings of the bot.
enum class PlayerState
{
	none,
	resting,
	attacking,
	pathing_to_quest,
	pathing,
	fetching,
};

namespace Globals
{
	extern HWND hwndFlyff;
	// loop globals
	extern bool initialized, exhaust_pause, questing;
	extern PlayerState player_state;
	extern std::chrono::system_clock::time_point exhaust_timer;
	// farm globals
	extern std::chrono::system_clock::time_point attack_pause_timer, move_check_timer, mp_potion_timer, fp_potion_timer;
	extern bool paused, check_rest, pathing;
	extern int fail_count, kill_count, stuck_count;
	extern Entity * farming_target;
	extern EntityQuest * target_quest;
	// input globals
	extern std::vector<std::pair<int, int>> target_mob_type, desired_items;
	extern bool ignore_items;
	extern std::pair<int, int> target_mob_levels;

	bool IsPlayerPathingToQuest( );

	void SetPlayerState( PlayerState state );
}