#include "flyff_structs.hpp"
#include "wings_helper.hpp"

namespace Pathing
{
	// use TRIG to align ourselves to a target point.
	void RotateYawToPoint( vec3 point );
	// Move to a random location within R units
	void MoveRandom( int radius );
	bool handle_stuck_pathing( );
	void check_jump_required( Entity * pLocal, Entity * pTarget );
	// Check if we are within questing area.
	bool check_reached_quest( );
	// Equip board and fly to a quest.
	void FlyToQuest( QuestProperty * pQuest );
	// Fly to quest, or walk if incapable. ( check for flight every 10s )
	void PathToQuest( QuestProperty * pQuest );
	// Fly to a specific point
	void FlyToPoint( vec3 point );
	// Get off board and also clear mouse target dest.
	void Reset( );
}