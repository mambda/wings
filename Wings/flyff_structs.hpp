#pragma once
#include "../../../generic_header.hpp"

struct Entity;

enum class Element : int
{
	Neutral,
	Fire,
	Water,
	Electricity,
	Wind,
	Stone
};

struct vec3
{
	float x, y, z;

	vec3( );
	vec3( float all );
	vec3( float x, float y, float z );

	vec3 operator+( vec3 other );
	vec3 operator-( vec3 other );

	float distance( vec3 & other );
	// distnace of x and z
	float distance2d( vec3 & other );
	bool is_zero( );
	void zero( );
};

struct vec2
{
	float x, y;

	vec2( );
	vec2( float all );
	vec2( float x, float y );

	vec2 operator+( vec2 other );
	vec2 operator-( vec2 other );

	float distance( vec2 & other );
	// distnace of x and z
	float distance2d( vec2 & other );
	bool is_zero( );
	void zero( );
};

struct EntityTarget
{
	char _pad[ 0x20 ];
	Entity * pTargetEntity;
	int target_id;
};

struct ItemProperty
{
	enum ItemType : int
	{
		mana_potion = 11, // or 25 in item_type 2
		vita_drink = 12, // fp , or 35 in item type two
		food = 14, // hp , or 30 in item type 2
	};

	int object_id;
	char item_name[ 0x20 ]; // null terminated
	char _pad[ 0x6C - 0x24 ]; // 0x24
	int max_stack;
	int unk;
	ItemType item_type_one;
	int item_type_two;
	char _pad2[ 0xE4 - 0x7C ];
	int low_attack;	// lowest this weapon can hit for
	int high_attack; // 0xE8 - highest this weapon can hit for
	char _pad3[ 0x158 - 0xEC ];
	int heal_amount; // 0x158 - how much does this item heal HP?
};

struct SkillDescriptor
{
	int32_t skill_obj_id; //0x0000
	int32_t skill_ent_id; //0x0004 i.e. these are all mental strikes
	int32_t skill_level; //0x0008
	char pad_000C[ 72 ]; //0x000C
	int32_t mp_cost; //0x0054
	int32_t fp_cost; //0x0058
	char pad_005C[ 48 ]; //0x005C
public:


	bool HasMPCost( );
	bool HasFPCost( );
};

// More detailed than EntitySkills.
struct Skill
{
	enum class CastDistance : int
	{
		One = 1,
		Three,
		Four,
		Ten,
		Fifteen,
		Six,
		Eighteen
	};

	enum class TargetType : int
	{
		Self = 1,
		Enemy = 4
	};

	enum SkillType
	{
		Buff,
		Single, // single target attack
		AOE,
		UNK
	};

	int id;
	char name[ 40 ];
	char _pad2[ 0xF0 - 0x2C ];
	Element skill_element; // 0xF0
	char _pad3[ 0x138 - 0xF4 ];
	CastDistance skill_distance;
	char _pad[ 0x1F4 - 0x13C ];
	int ability_type;
	TargetType target_type;
	char _pad12[ 0x240 - 0x1FC ];
	int gnames_id;

public:
	SkillType GetSkillType( );
	bool IsAttackSkill( );
	float GetCastDistance( );
};

struct EntitySkill
{
	int entity_skill_id;
	int points_given; // points put into this skill
};

struct EntityAttack
{
	enum class AttackType : int
	{
		None,
		Auto,
		MobAttack = 4,
		Skill = 5,
		Pickup,
		MAX,
	};

	AttackType attack_type;

	union
	{
		struct
		{
			int target_id; // entity unique id
		} autoattack;

		struct
		{
			int entity_skill_index; // where it shows up in the entity skill array.
			int target_id;
			int skill_id;
			AttackType current_attack;		// are we attacking anyone??
			int current_target; // who we are currently attacking
			int unk[ 4 ];
		} skill_attack;

		struct
		{
			int all_zero[ 3 ];
			AttackType real_attack_type;		// are we attacking anyone??
			int buff_id;
			int buff_target_id;
			int also_zero;
			int buff_level;
			int also_zero_2;
		} buff;
	};
};

struct PlayerInput
{
	enum Translation : unsigned char
	{
		still = 1,
		forward = 4,
		back = 5
	};

	enum Rotation : unsigned char
	{
		left = 1,
		right = 2,
		up = 4,
		down = 8,
		jump = 0x50,
	};

	enum Movement : unsigned char
	{
		running = 1,
		walking = 3,
		hovering = 9,
		fly = 25
	};

	void * vtable;
	unsigned char translation_flags; // 1 = still, 4 = forward, 5 = back
	unsigned char rotation_flags; // 1 = left 2 = right, 4 = pitch up, 8 = pitch down
	unsigned char damage_flags;
	unsigned char attack_input; // 1 during attack, 0 during nothing.
	unsigned char move_state; // 1 = ground, 9 = hovering, 25 = flying forward
};

struct InventoryItem
{
	char _pad[ 4 ];
	int inventory_id; // where is this in the big inventory array
	int inventory_slot_id; // where is it ingame
	int object_id;
	char _pad2[ 120 ];
	short quantity;
	short unk;
	char _pad3[ 60 ];

public:

	void UseItem( int target_id = -1, int using_id = -1, int to_use = 1 );

};

struct Inventory
{
	InventoryItem pItems[ 73 ];
};

struct InventoryInfo
{
	char _pad[ 4 ];
	int ** item_ids; // array of item ids in sequential order
	int max_inv_slots;
	int max_player_slots;
	Inventory * pInventory;
};

struct EntityQuest
{
	int unk;
	short quest_obj_id;
	uint8_t mob_deaths[ 3 ];
	uint8_t unk2;
	short unk3;
};

struct EntityQuestInformation
{
	short unk;
	short active_quests;
	EntityQuest * quests;
};

struct Buff
{
	void ** vtbl;
	char _pad[ 0x2 ];
	short buff_skill_id;
	int buff_level;
	int buff_max_duration; // in MS
	int buff_cast_time; // in MS

};

struct Entity
{
	enum class ObjectType : int
	{
		item = 4,
		entity = 5,
		MAX
	};

	enum class EntityJob
	{
		assist = 3,
		unk,
	};

	char _pad[ 0x18 ];
	float yaw; // Degrees
	float pitch; // Degrees
	char ____2pad[ 0x54 - 0x20 ];
	vec3 pos; // 0x54
	char _pad_[ 0x16C - 0x60 ];
	EntityTarget * pEntityTarget; // 0x16C
	ObjectType object_type; // entity, item, other shit.
	int object_id; // Mobs of same name, items of same name, etc.
	void * _pEntInformation;
	char _pad1[ 0x194 - 0x17C ];
	Entity ** pEntityList; // 0x194
	char ___pad3[ 0x2B8 - 0x198 ];
	int entity_type; // 0x2B8
	char __pad[ 0x2CC - 0x2BC ];
	float distance_from_cam; // 0x2CC
	char _____pad1[ 0x2F4 - 0x2D0 ];
	int entity_id; // 2F4
	char _pad2[ 0x33C - 0x2F8 ];
	int isLocalPlayer; // 0x33C
	int _whocares;
	PlayerInput * pPlayerInput;
	char _pad3[ 0x354 - 0x348 ];
	int is_channeling_item; // so we dont spam return scrolls for example.
	int unk[ 2 ];
	float skill_cast_distance; // how far away can we cast this .
	int target_entity_id; // set this to follow and to begin attacking
	vec3 mouse_target_dest; // 0x368
	char _pad4[ 0x380 - 0x374 ];
	int new_button_press; // set to -1 when doing shit. 380
	char _pad4__[ 0x400 - 0x384 ];
	uintptr_t * buff_manager; // 0x400
	char __pad4__[ 0x474 - 0x404 ];
	EntitySkill pSkills[ 51 ]; // All skills available to entity
	// 60C
	char _pad5[ 0x6D8 - 0x60C ];
	// entity stats
	int strength;
	int stamina;
	int dexterity;
	int intelligence;
	// entity stats
	char __pad_[ 0x71C - 0x6E8 ];
	int entity_level; // 71C
	char ____pad[ 8 ];
	unsigned long long exp; // dunno where to find maxes yet.
	char _pad6[ 0x760 - 0x730 ];
	int hp;
	char _pad7[ 0x788 - 0x764 ];
	int mp;
	int fp;
	int available_stat_points; // for int, dex, etc.
	char _pad8[ 0x7C8 - 0x794 ];
	EntityJob entity_job; // 0x7C8
	char _____pad[ 0x7F4 - 0x7CC ];
	int is_mob_aggro; // 0x7F4
	int mob_id; // 0x7F4
	char _pad11[ 0x804 - 0x7FC ];
	uint32_t elapsed_time;
	char __pad11[ 0x834 - 0x808 ];
	int mob_target_id; // what entity id is this mob targetting
	char _pad12[ 0xC18 - 0x838 ];
	InventoryInfo pInventoryInfo; // 0xC18
	char _pa_d[ 0xEB0 - 0xC2C ]; // 0xC2C
	EntityQuestInformation pQuestInformation; // 0xEB0
	char _pa_d2[ 0xF30 - 0xEB8 ]; // 0xC2C
	int is_casting_skill; // 0xF30
	char _pa_d_[ 0xFBC - 0xF34 ]; // F34
	int total_skillpoints; // how many skill points we have earned (for actual abilities) // 0xFBC
	int avail_skillpoints; // how many we can USE now
	char _pad9[ 0xFE8 - 0xFC4 ];
	int can_cast_skill; // 1 if true, 0if false.
	char _pad9_[ 0x10B0 - 0xFEC ];
	EntityAttack pAttack;
	int ______hahaha;
	// Buffs
	int str_buff;
	int dex_buff;
	int int_buff;
	int sta_buff;
	int _unk[ 6 ];
	int move_speed_buff;
	int _unk2[ 2 ];
	int melee_block_buff;
	int _unk3[ 20 ];
	int max_hp_buff; // in raw number
	int _unk4[ 11 ];
	int hit_chance_buff;
	char _pad10[ 0x11BC - 0x1198 ];
	int on_hit_buff; // 12 = Stun chance. 0x11BC
	char _pad11_[ 0x1528 - 0x11C0 ];
	int penya;
	char _________pad[ 0x1534 - 0x152C ];
	char entity_name[ ];

public:
	bool IsValid( );
	bool IsAlive( );
	bool IsNPC( );
	bool IsMob( );
	unsigned long long GetMaxEXP( );
	float GetFP( );
	float GetMP( );
	float GetHP( );
	float GetMaxHP( );
	float GetMaxMP( );
	float GetMaxFP( );
	int FindSkillIndex( EntitySkill * pSkill );
	void PickupItem( Entity * item );

	Buff * GetBuffByIndex( int index );
	Buff * HasBuff( std::string buff_name );

	// Assuming this is an item.
	ItemProperty * GetItemPropertyForItem( );
};
static_assert( sizeof( Entity ) == 0x1538, "Entity wrong size! %d" );

// 00C19C94 (base + 0x819C94)
struct LocalPlayerManager
{
	Entity * pLocalPlayer;
};

struct MobQuestPool
{
	int unique_drop_count;
	//everything after this is the end really.
	struct ItemPool
	{
		int some_index;
		int seems_zero;
		int item_object_id;
		int idk_drop_chance_i_guess;
		int idk_is_active_i_guess;
	};

	ItemPool pool[ 10 ]; // max count of 4 perhaps?
};

class MobProperty
{
public:
	int32_t object_id; //0x0000
	char name[ 32 ]; //0x0004
	char pad_0024[ 100 ]; //0x0024
	int mob_level; // 0x88 
	char _pad008C[ 28 ]; // 0x8C
	int dmg_lo; // 0xA8
	int dmg_max; // 0xAC
	char _pad00B0[ 0xFC - 0xB0 ]; // 0xB0
	Element mob_element; //0x00FC
	char pad_0100[ 0x2BC - 0x100 ]; //0x0100
	MobQuestPool pQuestDrops; // 2BC
	char pad_0388[ 0x690 - 0x388 ]; //0x388
}; //Size: 0x0690

static_assert( sizeof( MobProperty ) == 0x690, "MobProperty wrong size! %d" );

struct GNamesBite
{
	char _pad[ 4 ];
	void * pFirst;
	void ** pData;
	char _pad2[ 12 ];
	int entries;
	int unk[ 2 ];
};

struct QuestInformation
{
	int unk[ 2 ];
	int desired_object_id;
	int desired_object_quantity;
	int unk2[ 3 ];
	vec2 objective_pos;
	int unk3[ 3 ];
};

struct QuestProperty
{
	short quest_object_id;
	char quest_name[ 38 ];
	char _pad[ 240 ]; // 0x28
	QuestInformation * drop_info; // 118
	int unk2;
	int target_mob_ids[ 3 ]; // up to 3 specific monsters to kill
	int target_mob_kill_count[ 3 ]; // how many times to kill mob?
	struct whocares
	{
		int unk_number;
		vec2 pos;
		int unk_number_2;
	};

	whocares target_mob_locations[ 3 ];
	char _pad2[ 0x1D4 - 0x168 ]; // 168
	vec2 quest_handin_pos;
	char _pad4[ 0x3D8 - 0x1DC ]; // 168
	char * quest_description; // 3D8
	char _pad3[ 52 ];
	char * quest_success_response;
	int unk;
};

static_assert( sizeof( QuestProperty ) == 0x418, "QuestProperty wrong size! %d" );

struct GNames
{
	char _pad[ 472 ];
	int max_mobs;
	MobProperty * mobs;
	GNamesBite something_callout;
	GNamesBite interactables;
	GNamesBite motions;
	GNamesBite items;
	GNamesBite skill_properties;
	GNamesBite skill_descriptions;
	GNamesBite game_strings;
	char _pad2[ 24 ];
	GNamesBite quest_properties;
public:
	MobProperty * GetMobProperty( int mob_object_id );
	Skill * GetSkillProperty( int skill_id );
	SkillDescriptor * GetSkillDescriptor( EntitySkill * pSkill );
	ItemProperty * GetItemPropertyForID( int object_id );
	QuestProperty * GetQuestPropertyForID( int object_id );
};

class WorldArea
{
	char _pad[ 0x80C ];
public:
	void ** unk_0_obj_type;
	void ** unk_1_obj_type;
	void ** unk_2_obj_type;
	void ** unk_3_obj_type;
	Entity ** pItemList;
	Entity ** pEntityList;
	void ** unk_6_obj_type;
	void ** unk_7_obj_type;

	int max_0;
	int max_1;
	int max_2;
	int max_3;
	int active_items;
	int active_entities;
	int max_6;
	int max_7;
};

class WorldIdk
{
	char _pad[ 0x18 ];
public:
	WorldArea * GetAreaByObjectPosition( Entity * obj );
};

class World
{
	char _pad[ 0x10 ];
public:
	WorldIdk * pInternal;
};

struct MapObjects
{
	int mobs_count;
	int unk;
	int * mob_ids;
	int unk2[ 2 ];
	int item_id_dropped;
};

struct MapManager
{
	void * vtable;
	int max_entries;
	int unk[ 5 ];
	MapObjects ** objects;
};

class MapPtr
{
	char _pad[ 0x1DC4 ];
public:
	void * important_map_shit;

public:
	MapManager * GetMapManager( int id );

};

// Entity list to be found @ CA0640 ( base +  0x8A063F) )
// 00ac00c0 Global names. Base + 0x6C00C0

extern LocalPlayerManager * pLocalPlayerManager;
extern GNames * pGNames;
extern Entity ** pInViewList; // pEntityList, ** 
extern World * pWorld; // 00CBED70
extern MapPtr * pMapPtr;