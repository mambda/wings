#include "flyff_structs.hpp"
#include <cassert>

vec3::vec3( )
{
	this->x = this->y = this->z = 0;
}

vec3::vec3( float all )
{
	this->x = this->y = this->z = all;
}

vec3::vec3( float x, float y, float z )
{
	this->x = x;
	this->y = y;
	this->z = z;
}

vec3 vec3::operator+( vec3 other )
{
	vec3 out;
	out.x = this->x + other.x;
	out.y = this->y + other.y;
	out.z = this->z + other.z;
	return out;
}

vec3 vec3::operator-( vec3 other )
{
	vec3 out;
	out.x = this->x - other.x;
	out.y = this->y - other.y;
	out.z = this->z - other.z;
	return out;
}

float vec3::distance( vec3 & other )
{
	return sqrt( pow( this->x - other.x, 2 ) + pow( this->y - other.y, 2 ) + pow( this->z - other.z, 2 ) );
}

float vec3::distance2d( vec3 & other )
{
	return sqrt( pow( this->x - other.x, 2 ) + pow( this->z - other.z, 2 ) );
}

void vec3::zero( )
{
	this->x = 0.f;
	this->y = 0.f;
	this->z = 0.f;
}

bool vec3::is_zero( )
{
	return this->x == 0.f && this->y == 0.f && this->z == 0.f;
}

// VEC2
vec2::vec2( )
{
	this->x = this->y = 0.f;
}

vec2::vec2( float all )
{
	this->x = this->y = all;
}

vec2::vec2( float x, float y )
{
	this->x = x;
	this->y = y;
}

vec2 vec2::operator+( vec2 other )
{
	vec2 out;
	out.x = this->x + other.x;
	out.y = this->y + other.y;
	return out;
}

vec2 vec2::operator-( vec2 other )
{
	vec2 out;
	out.x = this->x - other.x;
	out.y = this->y - other.y;
	return out;
}

float vec2::distance( vec2 & other )
{
	return sqrt( pow( this->x - other.x, 2 ) + pow( this->y - other.y, 2 ) );
}

void vec2::zero( )
{
	this->x = 0.f;
	this->y = 0.f;
}

bool vec2::is_zero( )
{
	return this->x == 0.f && this->y == 0.f;
}

bool Entity::IsValid( )
{
	return this->entity_id != 0;
}

bool Entity::IsAlive( )
{
	return this->hp > 0;
}

bool Entity::IsNPC( ) // like town NPCs
{
	return this->mob_id == -1;
}

bool Entity::IsMob( )
{
	return this->mob_id != -1 && this->object_type == ObjectType::entity;
}

unsigned long long Entity::GetMaxEXP( )
{
	//0x00511370, 
	// 8b 45 fc 8b 88 1c 07 0 0 - 7
	//static auto fn = ( unsigned long long( __fastcall * )( Entity * ) ) ( ( uintptr_t )GetModuleHandleA( 0 ) + 0x111370 );
	using fn_t = unsigned long long( __fastcall * )( Entity * );
	static fn_t fn = nullptr;//  ( ( uintptr_t )GetModuleHandleA( 0 ) + 0x325A20 );
	if ( fn == nullptr )
	{
		auto ret = Utility::FindPattern( GetModuleHandleA( 0 ), "8b 45 fc 8b 88 1c 07 0 0" );
		if ( !ret )
			assert( "Failed to find pattern." );
		fn = ( fn_t )( ret - 7 );
	}
	return fn( this );
}

float Entity::GetFP( )
{
	return ( float )this->fp;
}

float Entity::GetMP( )
{
	return ( float )this->mp;
}

float Entity::GetHP( )
{
	return ( float )this->hp;
}

float Entity::GetMaxHP( )
{
	//83 ec 24 89 4d e4 8b 45 e4 50

	//return this->stamina * 14.3;
	using fn_t = int( __fastcall * )( Entity *, void *, int );
	static fn_t fn = nullptr;//  ( ( uintptr_t )GetModuleHandleA( 0 ) + 0x325A20 );
	if ( fn == nullptr )
	{
		auto ret = Utility::FindPattern( GetModuleHandleA( 0 ), "83 ec 24 89 4d e4 8b 45 e4 50" );
		if ( !ret )
			assert( "Failed to find pattern." );
		fn = ( fn_t )( ret - 3 );
	}
	auto max = ( float )fn( this, 0, 0 );
	max += this->max_hp_buff;

	return max;
}

float Entity::GetMaxMP( )
{
	using fn_t = int( __fastcall * )( Entity * );
	static fn_t fn = nullptr;//  ( ( uintptr_t )GetModuleHandleA( 0 ) + 0x325A20 );
	if ( fn == nullptr )
	{
		auto ret = Utility::FindPattern( GetModuleHandleA( 0 ), "c7 45 f8 0 0 80 3f 6a 00" );
		if ( !ret )
			assert( "Failed to find pattern." );
		fn = ( fn_t )( ret - 9 );
	}

	return fn( this );
}

float Entity::GetMaxFP( )
{
	//00725a20
	//0x325A20
	using fn_t = int( __fastcall * )( Entity * );
	static fn_t fn = nullptr;
	if ( fn == nullptr )
	{
		auto ret = Utility::FindPattern( GetModuleHandleA( 0 ), "c7 45 f8 0 0 80 3f 6a 0 8b 4d f0 e8 ? ? ? ? 50 6a 25" );
		if ( !ret )
			assert( "Failed to find pattern." );
		fn = ( fn_t )( ret - 9 );
	}

	return fn( this );
}

int Entity::FindSkillIndex( EntitySkill * pSkill )
{
	for ( int i = 0; i < _countof( this->pSkills ); i++ )
	{
		if ( this->pSkills[ i ].entity_skill_id == pSkill->entity_skill_id )
			return i;
	}

	LogLn<red>( "Cant find Skill %d in entity %p!", pSkill->entity_skill_id, this );
	return -1;
}

void Entity::PickupItem( Entity * item )
{
	//007210a0
	//0x3210A0
	// 85 c0 74 13 68 0 0 a0 41 - 0x1D
	//static auto fn = (  ) ( ( uintptr_t )GetModuleHandleA( 0 ) + 0x3210A0 );
	using fn_t = void( __fastcall * )( Entity *, void *, Entity * );
	static fn_t fn = nullptr;
	if ( fn == nullptr )
	{
		auto ret = Utility::FindPattern( GetModuleHandleA( 0 ), "85 c0 74 13 68 0 0 a0 41" );
		if ( !ret )
			assert( "Failed to find pattern." );
		fn = ( fn_t )( ret - 0x1D );
	}

	return fn( this, 0, item );
}

Buff * Entity::GetBuffByIndex( int index )
{
	//eb 08 8d 4d bc e8
	using fn_t = Buff * ( __fastcall * )( uintptr_t * );
	static fn_t fn = nullptr;
	if ( fn == nullptr )
	{
		auto ret = Utility::FindPattern( GetModuleHandleA( 0 ), "eb 08 8d 4d bc e8" );
		if ( !ret )
			assert( "Failed to find pattern." );
		auto base = ( ret + 5 );
		fn = ( fn_t )( base + *( uintptr_t * )( base + 1 ) + 5 );
	}

	uintptr_t setup = *this->buff_manager;

	for ( int i = 0; i < index; i++ )
	{
		if ( setup == 0 )
			return nullptr;

		fn( &setup );
	}

	if ( * ( unsigned char * )( setup + 0x15 ) != 0 )
		return nullptr;

	return *( Buff ** )( setup + 0x10 );
}

Buff * Entity::HasBuff( std::string buff_name )
{
	for ( int i = 0; ; i++ )
	{
		auto buff = this->GetBuffByIndex( i );
		if ( !buff )
			return nullptr;

		auto prop = pGNames->GetSkillProperty( buff->buff_skill_id );
		if ( !prop )
			return nullptr;

		if ( !_stricmp( prop->name, buff_name.c_str( ) ) )
			return buff;
	}

	return nullptr;
}

ItemProperty * Entity::GetItemPropertyForItem( )
{
	// not really siggable.
	//0044eab0
	//0x4EAB0
	if ( this->object_type != ObjectType::item )
		return nullptr;

	//static auto fn = ( ItemProperty * ( __fastcall * )( Entity * ) ) ( ( uintptr_t )GetModuleHandleA( 0 ) + 0x4EAB0 );
	//return fn( this );

	auto start = &pGNames->items;
	if ( start->entries < this->object_id )
		return nullptr;

	return ( ItemProperty * )start->pData[ this->object_id ];
}

ItemProperty * GNames::GetItemPropertyForID( int object_id )
{
	//0044b4d0
	//using fn_t = ItemProperty * ( __fastcall * )( GNames *, void *, int );
	//static fn_t fn = nullptr;
	//if ( fn == nullptr )
	//{
	//	auto ret = Utility::FindPattern( GetModuleHandleA( 0 ), "83 7d 08 0 7c 13 8b 4d fc 81 c1 4c" );
	//	if ( !ret )
	//		assert( "Failed to find pattern." );
	//	fn = ( fn_t )( ret - 7 );
	//}

	//return fn( this, 0, object_id );
	auto start = &pGNames->items;
	if ( start->entries < object_id )
		return nullptr;

	return ( ItemProperty * )start->pData[ object_id ];
}

QuestProperty * GNames::GetQuestPropertyForID( int object_id )
{
	auto start = &pGNames->quest_properties;
	if ( start->entries < object_id )
		return nullptr;

	return ( QuestProperty * )start->pData[ object_id ];

}

MobProperty * GNames::GetMobProperty( int mob_object_id )
{
	if ( mob_object_id > max_mobs )
		return nullptr;

	return &this->mobs[ mob_object_id ];
}

Skill * GNames::GetSkillProperty( int skill_id )
{
	//0044b700
	// lookup "GetSkillProp range_error"
	//83 7d 08 0 7c 13 8b 4d fc 81 c1 70 02 0 0
	//static auto fn = (  ) ( ( uintptr_t )GetModuleHandleA( 0 ) + 0x4B700 );
	//using fn_t = Skill * ( __fastcall * )( GNames *, void *, int );
	//static fn_t fn = nullptr;
	//if ( fn == nullptr )
	//{
	//	auto ret = Utility::FindPattern( GetModuleHandleA( 0 ), "83 7d 08 0 7c 13 8b 4d fc 81 c1 70 02 0 0" );
	//	if ( !ret )
	//		assert( "Failed to find pattern." );
	//	fn = ( fn_t )( ret - 7 );
	//}

	//return fn( this, 0, skill_id );
	auto start = &pGNames->skill_properties;
	if ( start->entries < skill_id )
		return nullptr;

	return ( Skill * )start->pData[ skill_id ];
}

SkillDescriptor * GNames::GetSkillDescriptor( EntitySkill * pSkill )
{
	//0069fbc0
	//89 4d f8 83 7d 0c 01 73 07
	//static auto fn = ( SkillDescriptor * ( __fastcall * )( GNames *, void *, int gname_id, int skill_level ) ) ( ( uintptr_t )GetModuleHandleA( 0 ) + 0x29FBC0 );
	using fn_t = SkillDescriptor * ( __fastcall * )( GNames *, void *, int gname_id, int skill_level );
	static fn_t fn = nullptr;
	if ( fn == nullptr )
	{
		auto ret = Utility::FindPattern( GetModuleHandleA( 0 ), "89 4d f8 83 7d 0c 01 73 07" );
		if ( !ret )
			assert( "Failed to find pattern." );
		fn = ( fn_t )( ret - 6 );
	}

	auto skill_real = this->GetSkillProperty( pSkill->entity_skill_id );
	return fn( this, 0, skill_real->gnames_id, pSkill->points_given );
}

Skill::SkillType Skill::GetSkillType( )
{
	switch ( this->ability_type )
	{
		case 2:
		case 8:
		case 9:
			return SkillType::Buff;
		case 13:
			return SkillType::AOE;
		case 17:
			return SkillType::Single;
		default:
			return SkillType::UNK;
	}
}

bool Skill::IsAttackSkill( )
{
	auto type = this->GetSkillType( );
	return  ( type == SkillType::AOE || type == SkillType::Single );
}

bool SkillDescriptor::HasMPCost( )
{
	return this->mp_cost != -1;
}

bool SkillDescriptor::HasFPCost( )
{
	return this->fp_cost != -1;
}

void InventoryItem::UseItem( int target_id, int using_id, int to_use )
{
	//	c1 e0 10 50 b9
	using fn_t = void( __fastcall * )( void *, void *, int, int, int, int );
	static fn_t fn = nullptr; // 0042a0b0
	static void * packet_base = nullptr;
	if ( fn == nullptr )
	{
		auto ret = Utility::FindPattern( GetModuleHandleA( 0 ), "c1 e0 10 50 b9" );
		if ( !ret )
			assert( "Failed to find pattern." );
		packet_base = *( void ** )( ret + 5 ); // 
		auto temp = *( uintptr_t * )( ret + 0xA );  // 006362fe
		fn = ( fn_t )( temp + ( ret + 0x9 ) + 0x5 );
	}

	return fn( packet_base, 0, this->inventory_id << 0x10, target_id, using_id, to_use );
}

WorldArea * WorldIdk::GetAreaByObjectPosition( Entity * obj )
{
	//8b 08 89 4d ec 8d 55 d4 52 
	using fn_t = WorldArea * ( __fastcall * )( void *, void *, Entity * );
	static fn_t fn = nullptr; // 
	if ( fn == nullptr )
	{
		auto ret = Utility::FindPattern( GetModuleHandleA( 0 ), "8b 08 89 4d ec 8d 55 d4 52" );
		if ( !ret )
			assert( "Failed to find pattern." );
		fn = ( fn_t )( ret - 0x15 );
	}

	return fn( this, 0, obj );
}

float Skill::GetCastDistance( )
{
	switch ( this->skill_distance )
	{
		case CastDistance::One:
			return 1.0f;
		case CastDistance::Three:
			return 3.f;
		case CastDistance::Four:
			return 4.f;
		case CastDistance::Six:
			return 6.f;
		case CastDistance::Ten:
			return 10.f;
		case CastDistance::Fifteen:
			return 15.f;
		case CastDistance::Eighteen:
			return 18.f;
		default:
			return 0.f;
	}
}

MapManager * MapPtr::GetMapManager( int id )
{
	using asdf = MapManager * ( __fastcall * )( MapPtr *, void *, int );
	static asdf fn = nullptr;
	if ( !fn )
	{
		auto ret = Utility::FindPattern( GetModuleHandleA( 0 ), "8b 91 a8 10 0 0 52 b9" );
		ret += 12;
		fn = ( decltype( fn ) )( ret + *( uintptr_t * )( ret + 1 ) + 5 );
	}

	return fn( this, 0, id );
}
