#include "wings_helper.hpp"
#include "wings_core.hpp"
#include "PathingHandler.hpp"
#include "CombatHandler.hpp"
#include "QuestHandler.hpp"
#include "InputHandler.hpp"
#include "JobHandlers.hpp"

namespace Globals
{
	// loop globals
	HWND hwndFlyff = 0;
	bool initialized = false, exhaust_pause = false, questing = false;
	PlayerState player_state = PlayerState::resting;
	std::chrono::system_clock::time_point exhaust_timer;
	// farm globals
	std::chrono::system_clock::time_point attack_pause_timer, move_check_timer, mp_potion_timer, fp_potion_timer;
	bool paused = false, check_rest = false;
	int fail_count = 0, kill_count = 0, stuck_count = 0;
	Entity * farming_target = nullptr;
	EntityQuest * target_quest = nullptr;
	// input globals
	std::vector<std::pair<int, int>> target_mob_type, desired_items;
	bool ignore_items = false;
	std::pair<int, int> target_mob_levels = { 2,1 };
}

LocalPlayerManager * pLocalPlayerManager;
World * pWorld;
GNames * pGNames;
MapPtr * pMapPtr;

namespace Wings
{
	void initialize( )
	{
		auto base = ( uintptr_t )GetModuleHandle( 0 );
		//8b 85 08 63 ff ff 3b 05 ? ? ? ?
		//auto ret = Utility::FindPattern( ( HMODULE )base, "8b 85 08 63 ff ff 3b 05" );
		//pInViewList = *( decltype( pInViewList ) * )( ret + 0x1B ); // old ent list.
		//89 4d fc a1 ? ? ? ? 2b
		auto ret = Utility::FindPattern( ( HMODULE )base, "89 4d fc a1 ? ? ? ? 2b" );
		pLocalPlayerManager = *( decltype( pLocalPlayerManager ) * )( ( ret + 4 ) );
		//pGNames = ( decltype( pGNames ) )( base + 0x6C00C0 );
		ret = Utility::FindPattern( ( HMODULE )base, "68 74 0f 0 0 b9" );
		pGNames = *( decltype( pGNames ) * )( ret + 6 );

		//c7 80 fc 0 0 0 0 0 70 41
		ret = Utility::FindPattern( ( HMODULE )base, "c7 80 fc 0 0 0 0 0 70 41" );
		pWorld = *( decltype( pWorld ) * )( ret + 0xB );

		//8b 91 a8 10 0 0 52 b9
		ret = Utility::FindPattern( ( HMODULE )base, "8b 91 a8 10 0 0 52 b9" );
		ret += 12;
		pMapPtr = *( decltype( pMapPtr ) * )( ret - 4 );
		Globals::initialized = true;

		//0f 84 8f 02 0 0 c7
		//ret = Utility::FindPattern( ( HMODULE )base, "0f 84 8f 02 0 0 c7" );
		//pEntityList = *( decltype( pEntityList ) * )( ret + 0xC );
		//c7 45 fc ff ff ff ff 81 3d
		//ret = Utility::FindPattern( ( HMODULE )base, "c7 45 fc ff ff ff ff 81 3d" );
		//active_entities_count = *( decltype( active_entities_count ) * )( ret + 9 );

		LogLn( "pMapPtr @ %p", pMapPtr );
		LogLn( "pWorld @ %p", pWorld );
		LogLn( "pLocalPlayerManager @ %p", pLocalPlayerManager );
		LogLn( "pGNames @ %p", pGNames );
		LogLn<gold>( "Wings initialized." );
	}

	void pickup_item( Entity * pLocal, Entity * target )
	{
		//static Entity * last = nullptr;
		Entity * item = nullptr;

		//if ( last != nullptr && last->IsValid( ) )
		//	item = last;
		//else
		item = target;

		auto item_prop = item->GetItemPropertyForItem( );

		Helper::SetTarget( item );
		//if ( last != item )
		//	last = item;
		//LogLn<gold>( "Picking up item %s [%p]", item_prop->item_name, item );

		pLocal->PickupItem( item );
	}

	// Make sure HP MP FP are fully restored.
	bool ensure_rested( Entity * pLocal )
	{
		auto maxhp = pLocal->GetMaxHP( );
		auto maxfp = pLocal->GetMaxFP( );
		auto maxmp = pLocal->GetMaxMP( );

		// everything over half.
		if ( pLocal->entity_job == Entity::EntityJob::assist )
			return Assist::HandleRest( );
		else
			return ( pLocal->hp >= maxhp && pLocal->mp >= ( maxmp / 2.f ) && pLocal->fp >= maxfp );
	}

	void check_inventory_for_desired_items( Entity * pLocal )
	{
		auto pInv = pLocal->pInventoryInfo.pInventory;
		for ( unsigned int i = 0; i < pLocal->pInventoryInfo.max_player_slots; i++ )
		{
			auto & cur = pInv->pItems[ i ];
			if ( cur.object_id == 0 )
				continue;

			for ( unsigned int j = 0; j < Globals::desired_items.size( ); j++ )
			{
				auto & x = Globals::desired_items[ j ];

				if ( x.second == -1 )
					continue;

				if ( x.first == cur.object_id )
				{
					if ( cur.quantity >= x.second )
					{
						LogLn<bright_green>( "Collected %d of %s.", cur.quantity, pGNames->GetItemPropertyForID( cur.object_id )->item_name );
						Globals::desired_items.erase( Globals::desired_items.begin( ) + j, Globals::desired_items.begin( ) + j + 1 );
					}
				}
			}
		}
	}

	bool is_inventory_full( )
	{
		auto pLocal = Helper::GetLocalPlayer( );
		if ( !pLocal || !pLocal->pInventoryInfo.pInventory )
			return false;

		for ( auto & x : pLocal->pInventoryInfo.pInventory->pItems )
			if ( x.object_id == 0 )
				return false;

		return true;
	}

	bool check_for_drops( Entity * pLocal )
	{
		if ( !pLocal || !pLocal->pInventoryInfo.pInventory )
			return false;

		if ( is_inventory_full( ) )
		{
			LogLn( "Inventory full. Pissofff." );
			return false;
		}

		auto item_lambda = [ ]( Entity * cur_item )->bool
		{
			if ( Globals::desired_items.front( ).first == 0xCAFEBABE )
				return true;
			else
			{
				for ( unsigned int i = 0; i < Globals::desired_items.size( ); i++ )
				{
					auto & x = Globals::desired_items[ i ];
					if ( x.first == cur_item->object_id )
						return true;
				}
			}

			return false;
		};

		Entity * target_item = nullptr;
		if ( Globals::desired_items.size( ) )
			target_item = Helper::FindNextItem( item_lambda, Globals::ignore_items );
		else
			target_item = Helper::FindNextItem( );

		if ( target_item != nullptr && Globals::farming_target == nullptr )
		{
			pickup_item( pLocal, target_item );
			Helper::ResetAFK( false );
			return true;
		}

		return false;
	}

	void main_loop( )
	{
		static int last_hp = 0;
		static int last_level = 0;

		if ( !Globals::initialized )
			initialize( );

		if ( pLocalPlayerManager->pLocalPlayer == nullptr )
			return;

		auto pLocal = pLocalPlayerManager->pLocalPlayer;
		pLocal->new_button_press = -1;

		if ( pLocal->hp <= 0 && last_hp > 0 )
		{
			last_hp = 0;
			Globals::SetPlayerState( PlayerState::resting );
			LogLn<bright_red>( "We died! :(" );
			Globals::farming_target = nullptr;
			Beep( 666, 500 );
			return; // for now. // TODO: Reset everything.
		}

		check_inventory_for_desired_items( pLocal );

		auto aggro_check = [ & ]( )
		{
			if ( pLocal->hp < last_hp )
			{
				//something is aggroing us.
				LogLn<red>( "Rest broken due to aggro! Player hp is %.0f / %.0f", pLocal->GetHP( ), pLocal->GetMaxHP( ) );
				last_hp = pLocal->hp;
				Combat::seek_and_kill_mobs( true );
				Globals::player_state = PlayerState::attacking;
				Helper::ResetAFK( false );
				return true;
			}

			return false;
		};

		// rest after quest
		if ( Quest::IsQuesting( ) )
		{
			// Get a new one.
			if ( Quest::HasQuest( ) == false )
			{
				Quest::SetQuest( Quest::GetNextQuest( ) );
				if ( Quest::HasQuest( ) )
				{
					Globals::SetPlayerState( PlayerState::pathing_to_quest );

					auto questInfo = Quest::GetCurrentQuestProperty( );
					Pathing::FlyToQuest( questInfo );

					// ALso, if in no fly zone, start walking :(

					//LogLn<gold>( "Starting quest %s", questInfo->quest_name );
				}
				else
				{
					LogLn<yellow>( "All quests are ready to be handed in!" );
					Globals::questing = false;
				}
			}
			else
			{
				auto questProp = Quest::GetCurrentQuestProperty( );
				if ( !Globals::paused && Globals::player_state == PlayerState::pathing_to_quest )
					Pathing::PathToQuest( questProp );
				//else
				//{
				//	if ( ( Globals::attack_pause_timer - std::chrono::system_clock::now( ) ).count( ) <= 0 )
				//		Globals::paused = false; // TODO: Make these timers look nicer lol.
				//}

				if ( Globals::IsPlayerPathingToQuest( ) )
				{
					if ( Pathing::check_reached_quest( ) )
					{
						Pathing::Reset( );

						Sleep( 3000 ); // wait 3s until we stop jumping

						if ( questProp->drop_info )
						{
							auto target_object = Helper::FindMapObjectForItemID( questProp->drop_info->desired_object_id );
							if ( target_object != nullptr )
							{
								Globals::target_mob_type.clear( );
								// -1 to avoid the boss mob.
								for ( int i = 0; i < target_object->mobs_count - 1; i++ )
								{
									LogLn<green>( "Targeting mob id %d for quest.", target_object->mob_ids[ i ] );
									Globals::target_mob_type.push_back( { target_object->mob_ids[ i ], -1 } );
								}
							}
							else
							{
								bool unset = true;
								// Not getting a regular mob drop, check if any nearby mobs contain the quest item in their quest pool.
								auto quest_check = [ & ]( Entity * pMob, Entity * pLocal )
								{
									auto prop = pGNames->GetMobProperty( pMob->object_id );
									if ( prop->pQuestDrops.unique_drop_count == 0 )
										return false;

									for ( int i = 0; i < prop->pQuestDrops.unique_drop_count; i++ )
										if ( prop->pQuestDrops.pool[ i ].item_object_id == questProp->drop_info->desired_object_id )
											return true;

									return false;
								};
								auto ret = Helper::FindClosestMob( quest_check ); // also check if we need any special drops.
								if ( ret != nullptr )
								{
									LogLn<green>( "Special drop required. Targeting mob %d (%s)", ret->object_id, ret->entity_name );
									Globals::target_mob_type.push_back( { ret->object_id, -1 } );
									unset = false;
								}
								else if ( unset )
								{
									LogLn<red>( "Cant figure out which mobs to attack. Must be a fetch quest." );
									Globals::target_mob_type.clear( );
									Globals::target_mob_type.push_back( { -1,-1 } ); // target no mobs by choice.
									Globals::SetPlayerState( PlayerState::fetching );
								}
							}
						}
						Sleep( 1500 );
						Helper::ResetAFK( false );
						return; // give time for us to dismount.
					}
				}
				else if ( Quest::IsQuestCompleted( ) )
				{
					LogLn<yellow>( "Finished farming for quest %s!", questProp->quest_name );
					Globals::SetPlayerState( PlayerState::none );
					Quest::ClearQuest( );
					Globals::target_mob_type.clear( );
				}
			}
		}

		// items after quest
		bool grabbing_item = false;

		// wont grab if pathing to quest. or if attacking a mob.
		auto item_grab = [ & ]( )
		{
			if ( Globals::IsPlayerPathingToQuest( ) || Globals::player_state == PlayerState::attacking )
				return;

			if ( Globals::paused )
			{
				if ( ( Globals::attack_pause_timer - std::chrono::system_clock::now( ) ).count( ) <= 0 )
				{
					grabbing_item = check_for_drops( pLocal );
					Globals::paused = false;
				}
			}
			else
				grabbing_item = check_for_drops( pLocal );

		};

		if ( Globals::player_state == PlayerState::resting )
		{
			if ( !aggro_check( ) )
			{
				last_hp = pLocal->hp;

				item_grab( );

				if ( ensure_rested( pLocal ) )
				{
					Globals::SetPlayerState( PlayerState::none );
					return;
				}
				else
				{
					//if ( !grabbing_item )
					//	Helper::ResetAFK( );
					return;
				}
			}
			else
				return;
		}
		else if ( Globals::player_state == PlayerState::fetching )
		{
			aggro_check( );
			item_grab( );
		}
		else
			item_grab( );

		// then do normal farming.
		if ( !Globals::exhaust_pause && !grabbing_item && !Helper::IsPlayerOccupied( ) )
		{
			// target died.

			auto ret = Combat::seek_and_kill_mobs( );
			if ( ret == farm_ret::kill )
			{
				auto max_exp = pLocal->GetMaxEXP( );
				auto pct = ( ( float )pLocal->exp / max_exp ) * 100.f;

				LogLn<yellow>( "Stats:\nHP: %.0f/%.0f\nMP: %.0f/%.0f\nFP: %.0f/%.0f\nEXP: %llu/%llu (%.2f%%)\nPenya: %d\nResting.\n\n",
							   pLocal->GetHP( ), pLocal->GetMaxHP( ),
							   pLocal->GetMP( ), pLocal->GetMaxMP( ),
							   pLocal->GetFP( ), pLocal->GetMaxFP( ),
							   pLocal->exp, max_exp, pct,
							   pLocal->penya );
				Globals::SetPlayerState( PlayerState::resting );
				check_for_drops( pLocal );
				last_hp = pLocal->hp;
				if ( last_level && pLocal->entity_level > last_level )
				{
					LogLn<bright_green>( "Level up to level %d!", pLocal->entity_level );
					Beep( 400, 500 );
				}

				last_level = pLocal->entity_level;
			}
			else if ( ret == farm_ret::exhausted && Quest::IsQuesting( ) == false )
			{
				LogLn<bright_red>( "This area has been exhausted of viable targets! Relocation recommended." );
				Globals::exhaust_pause = true;
				Globals::exhaust_timer = std::chrono::system_clock::now( ) + std::chrono::seconds( 5 );
				Globals::SetPlayerState( PlayerState::resting );
				Pathing::MoveRandom( 20.f );
				Beep( 333, 200 );
			}
			else if ( ret == farm_ret::none )
				Globals::SetPlayerState( PlayerState::resting );
			else if ( ret == farm_ret::engaged )
				Globals::SetPlayerState( PlayerState::attacking );
		}
		else
		{
			// Do stuck check while resting or otherwise as well. Makes us seem more human anyways.
			if ( ( Globals::move_check_timer - std::chrono::system_clock::now( ) ).count( ) <= 0/* && ( Globals::player_state != PlayerState::resting && Globals::player_state != PlayerState::none )*/ )
			{
				Globals::move_check_timer = std::chrono::system_clock::now( ) + std::chrono::milliseconds( 250 );
				if ( Pathing::handle_stuck_pathing( ) )
				{
					Globals::attack_pause_timer = std::chrono::system_clock::now( ) + std::chrono::milliseconds( 100 );// wait 2 seconds before next attempt.
					Globals::paused = true;
				}
			}

			// wait 1 minute before checking again.
			if ( ( Globals::exhaust_timer - std::chrono::system_clock::now( ) ).count( ) <= 0 )
				Globals::exhaust_pause = false;
		}
	}
}