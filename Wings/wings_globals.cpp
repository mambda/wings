#include "wings_globals.hpp"

namespace Globals
{
	bool IsPlayerPathingToQuest( )
	{
		return player_state == PlayerState::pathing_to_quest;
	}

	void SetPlayerState( PlayerState state )
	{
		player_state = state;
	}
}