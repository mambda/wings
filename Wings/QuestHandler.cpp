#include "QuestHandler.hpp"
#include "wings_helper.hpp"
#include "PathingHandler.hpp"

namespace Quest
{
	bool HasQuest( )
	{
		return Globals::target_quest != nullptr;
	}

	EntityQuest * GetNextQuest( )
	{
		auto pLocal = Helper::GetLocalPlayer( );
		for ( unsigned int i = 0; i < pLocal->pQuestInformation.active_quests; i++ )
		{
			auto cur = &pLocal->pQuestInformation.quests[ i ];
			if ( IsQuestCompleted( cur ) )
				continue;

			return cur;
		}

		return nullptr;
	}

	EntityQuest * GetCurrentQuest( )
	{
		return Globals::target_quest;
	}

	void SetQuest( EntityQuest * newQuest )
	{
		if ( newQuest == nullptr )
			return;
		Globals::target_quest = newQuest;
		auto prop = pGNames->GetQuestPropertyForID( newQuest->quest_obj_id );
		LogLn<yellow>( "Beginning quest %s [property: %p]", prop->quest_name, prop );

		if ( prop->target_mob_ids[ 0 ] != 0 )
		{
			for ( int i = 0; i < _countof( prop->target_mob_ids ); i++ )
			{
				auto id = prop->target_mob_ids[ i ];
				if ( id == 0 )
					break; // no more.

				int necessary_count = prop->target_mob_kill_count[ i ] - newQuest->mob_deaths[ i ];
				LogLn<green>( "Specific mob required. Targeting mob %d (%s) %d more times.", id, pGNames->GetMobProperty( id )->name, necessary_count );
				if ( necessary_count > 0 )
					Globals::target_mob_type.push_back( { id, necessary_count } );
				else
					LogLn<bright_green>( "Already done." );
			}
		}
	}

	bool IsQuestCompleted( EntityQuest * pQuest )
	{
		auto pLocal = Helper::GetLocalPlayer( );
		if ( !pQuest )
			pQuest = Globals::target_quest;
		if ( pQuest == nullptr )
			return true;

		auto prop = pGNames->GetQuestPropertyForID( pQuest->quest_obj_id );
		if ( !prop )
			return true;
		bool items_ready = false;
		bool mobs_ready = true;

		if ( prop->drop_info == nullptr )
			items_ready = true; // no items to collect.
		else
		{
			auto desired_item = prop->drop_info->desired_object_id;
			auto desired_item_quant = prop->drop_info->desired_object_quantity;

			auto item = Helper::GetInventoryItem( pLocal, desired_item );
			items_ready = ( item && item->quantity >= desired_item_quant );
		}

		for ( auto & x : Globals::target_mob_type )
			if ( x.second != -1 )
				mobs_ready = false;
		return items_ready && mobs_ready;
	}

	QuestProperty * GetCurrentQuestProperty( )
	{
		if ( HasQuest( ) == false )
			return nullptr;

		return pGNames->GetQuestPropertyForID( Quest::GetCurrentQuest( )->quest_obj_id );
	}

	vec3 GetDestination( )
	{
		auto pQuest = Quest::GetCurrentQuestProperty( );

		vec3 target_dest = { 0 };
		if ( pQuest->target_mob_ids[ 0 ] == 0 )
		{
			if ( pQuest->drop_info == nullptr )
			{
				LogLn<bright_red>( "QUEST DESTINATION ERROR" );
				return vec3( 0 );
			}

			if ( pQuest->drop_info->objective_pos.x == -1 )
			{
				// find item drop location on map.
				auto map_obj = Helper::FindMapObjectForItemID( pQuest->drop_info->desired_object_id );
				// TODO: Convert to world positions :(
				return vec3( 0 );
			}
			else
				target_dest = { pQuest->drop_info->objective_pos.x, 0, pQuest->drop_info->objective_pos.y };
		}
		else
		{
			auto cur = Globals::target_quest;
			for ( int i = 0; ; i++ )
			{
				if ( pQuest->target_mob_ids[ i ] != 0 )
				{
					if ( pQuest->target_mob_kill_count[ i ] <= cur->mob_deaths[ i ] )
						continue;

					// we have not yet killed this mob
					target_dest = { pQuest->target_mob_locations[ i ].pos.x, 0, pQuest->target_mob_locations[ i ].pos.y };
					break;
				}
			}
		}
		return target_dest;
	}

	void ClearQuest( )
	{
		Globals::target_quest = nullptr;
	}

	bool IsQuesting( )
	{
		return Globals::questing;
	}
}