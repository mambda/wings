#pragma once
#include "flyff_structs.hpp"
#include <functional>
#include <vector>

namespace Helper
{
	// Target one (in hud)
	void SetTarget( Entity * pTarget );
	Entity * GetTarget( Entity * pLocal );
	bool IsFollowingTarget( Entity * pLocal );
	// Follow an entity
	void FollowEntity( Entity * pTarget );
	bool IsVisible( Entity * target, Entity * pLocal = nullptr );
	// Get closest mob to an entity (localplayer by default)
	Entity * FindClosestMob( std::function<bool( Entity *, Entity * )> optional_considerations = nullptr, Entity * to = nullptr );
	Entity * FindNextItem( std::function<bool( Entity * )> optional_considerations = nullptr, bool ignore = false );
	Entity * GetLocalPlayer( );
	bool CanCastSkill( Entity * pLocal, EntitySkill * pSkill );
	void cast_skill( Entity * pTarget, EntitySkill * pSkill );
	bool DoesElementCounter( Element attacker, Element target );
	EntitySkill * find_skill_in_entity( Entity * ent, int id );
	EntitySkill * find_skill_in_entity( Entity * ent, std::string name );
	EntitySkill * choose_best_combat_skill( Entity * pLocal, Entity * pTarget );
	void do_autoattack( Entity * pTarget );
	void stop_attacking( );
	void MoveForward( Entity * pLocal );
	void SpinCharacter( Entity * pLocal, bool left = true );
	void MoveBackward( Entity * pLocal );
	void Jump( Entity * pLocal );
	bool IsPlayerOccupied( );
	// Sends input to game so it lets us do shit.
	void ResetAFK(bool down = true );

	InventoryItem * GetInventoryItem( Entity * pLocal, int item_id );
	MapManager * GetMapManagerForMapID( int id );
	MapObjects * FindMapObjectForItemID( int item_id );
	bool HasFlyingVehicle( );
	void EquipFlyingVehicle( bool mount = true );

	// Returns true if blocked, false if clear.
	bool CastRay( vec3 * hit_pos, vec3 * start, vec3 * end, int param4 = 0, int param5 = 0, int param6 = 1 );

	enum class heal_type
	{
		hp,
		mp,
		fp
	};

	enum class search_type
	{
		strongest,
		cheapest,
	};

	InventoryItem * FindHealingItem( Entity * pLocal, heal_type ht = heal_type::hp, search_type st = search_type::strongest );
	int GetMobIDForName( std::string mob_name );
	int GetItemIDForName( std::string item_name );
}