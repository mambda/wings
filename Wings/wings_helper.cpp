#include "wings_helper.hpp"
#include "InputHandler.hpp"

namespace Helper
{
	void SetTarget( Entity * pTarget )
	{
		auto pLocal = Helper::GetLocalPlayer( );
		if ( pLocal->pEntityTarget == nullptr )
			return;

		pLocal->pEntityTarget->pTargetEntity = pTarget;
	}

	Entity * GetTarget( Entity * pLocal )
	{
		if ( pLocal->pEntityTarget == nullptr )
			return nullptr;
		return pLocal->pEntityTarget->pTargetEntity;
	}

	bool IsFollowingTarget( Entity * pLocal )
	{
		return pLocal->target_entity_id != -1;
	}

	void FollowEntity( Entity * pTarget )
	{
		auto pLocal = Helper::GetLocalPlayer( );
		pLocal->target_entity_id = pTarget ? pTarget->entity_id : -1;
	}

	bool IsVisible( Entity * target, Entity * pLocal )
	{
		if ( !pLocal )
			pLocal = Helper::GetLocalPlayer( );

		vec3 out, start = pLocal->pos, end = target->pos;


		auto ret = Helper::CastRay( &out, &start, &end );
		return ret == false;
	}

	Entity * FindClosestMob( std::function<bool( Entity *, Entity * )> optional_considerations, Entity * to )
	{
		Entity * closest_ent = nullptr;
		if ( to == nullptr )
			to = pLocalPlayerManager->pLocalPlayer;

		auto pArea = pWorld->pInternal->GetAreaByObjectPosition( to );
		Entity * curEnt = pArea->pEntityList[ 0 ];

		float closest = FLT_MAX;
		int i = 0;

		for ( ; i < pArea->active_entities; i++ )
		{
			curEnt = pArea->pEntityList[ i ];
			if ( curEnt == nullptr )
				continue;
			// Fun fact, you CAN damage yourself lmfao.
			if ( curEnt->IsValid( ) == false || curEnt->IsAlive( ) == false || curEnt->IsMob( ) == false || curEnt == to )
				continue;

			if ( optional_considerations )
				if ( optional_considerations( curEnt, to ) == false )
					continue;

			if ( Helper::IsVisible( curEnt ) == false )
				continue;

			auto dist = curEnt->pos.distance( to->pos );
			if ( curEnt->is_mob_aggro )
				dist /= 1.6f; // rather chase down an aggro boy than leave them be.
			if ( dist < closest )
			{
				closest = dist;
				closest_ent = curEnt;
			}
		}
		//LogLn( "Checked %d entities", i );
		return closest_ent;
	}

	Entity * FindNextItem( std::function<bool( Entity * )> optional_considerations, bool ignore )
	{
		auto pArea = pWorld->pInternal->GetAreaByObjectPosition( pLocalPlayerManager->pLocalPlayer );
		auto pList = pArea->pItemList;
		Entity * curEnt = pList[ 0 ];
		//Entity * curEnt = pInViewList[ 0 ];

		for ( int i = 0; i < pArea->active_items; i++ )
		{
			curEnt = pList[ i ];
			if ( curEnt == nullptr )
				continue;
			// Fun fact, you CAN damage yourself lmfao.
			if ( curEnt->IsValid( ) == false || curEnt->object_type != Entity::ObjectType::item )
				continue;

			// if item far away, we didnt get it and i cba lol. (or is this bad for quest drop items?)
			if ( curEnt->pos.distance( pLocalPlayerManager->pLocalPlayer->pos ) > 80.f )
				continue;

			auto item = pGNames->GetItemPropertyForID( curEnt->object_id );
			if ( strstr( item->item_name, "Penya" ) || strstr( item->item_name, "penya" ) )
				return curEnt; // always pickup penya if in range.

			if ( optional_considerations )
				if ( optional_considerations( curEnt ) == ignore )
					continue;

			return curEnt;
		}
		//LogLn( "Checked %d entities", i );
		return nullptr;
	}

	Entity * GetLocalPlayer( )
	{
		return pLocalPlayerManager->pLocalPlayer;
	}

	bool CanCastSkill( Entity * pLocal, EntitySkill * pSkill )
	{
		auto skill = pGNames->GetSkillDescriptor( pSkill );
		auto info = pGNames->GetSkillProperty( pSkill->entity_skill_id );

		if ( pSkill->points_given <= 0 )
			return false;

		if ( skill->HasFPCost( ) )
			return ( pLocal->fp >= skill->fp_cost );
		else if ( skill->HasMPCost( ) )
			return ( pLocal->mp >= skill->mp_cost );
		else
			return true; // no known cost.
	}

	void cast_skill( Entity * pTarget, EntitySkill * pSkill )
	{
		auto pLocal = Helper::GetLocalPlayer( );

		if ( !pLocal || !pTarget || !pSkill || pSkill->entity_skill_id == -1 )
			return;

		auto real_skill = pGNames->GetSkillProperty( pSkill->entity_skill_id );
		//LogLn( "Using skill at %p", real_skill );

		pLocal->pAttack.attack_type = EntityAttack::AttackType::Skill;
		pLocal->pAttack.skill_attack.target_id = pTarget->entity_id;
		pLocal->pAttack.skill_attack.skill_id = 0;// pSkill->entity_skill_id;
		pLocal->pAttack.skill_attack.entity_skill_index = pLocal->FindSkillIndex( pSkill );
		// set follow and distance?
		if ( pTarget != pLocal )
			Helper::SetTarget( pTarget );
		Helper::FollowEntity( pTarget );
		pLocal->skill_cast_distance = real_skill->GetCastDistance( );
	}

	bool DoesElementCounter( Element attacker, Element target )
	{
		switch ( attacker )
		{
			case Element::Fire:
				return ( target == Element::Wind );
			case Element::Water:
				return ( target == Element::Fire );
			case Element::Electricity:
				return ( target == Element::Water );
			case Element::Stone:
				return ( target == Element::Electricity );
			case Element::Wind:
				return ( target == Element::Stone );
			default:
				return false;
		}
	}

	EntitySkill * find_skill_in_entity( Entity * ent, int id )
	{
		for ( int i = 0; i < _countof( ent->pSkills ); i++ )
		{
			auto cur = &ent->pSkills[ i ];
			if ( cur->entity_skill_id == -1 )
				break;

			if ( cur->entity_skill_id == id )
				return cur;
		}

		return nullptr;
	}

	EntitySkill * find_skill_in_entity( Entity * ent, std::string name )
	{
		for ( int i = 0; i < _countof( ent->pSkills ); i++ )
		{
			auto cur = &ent->pSkills[ i ];
			if ( cur->entity_skill_id == -1 )
				break;

			auto prop = pGNames->GetSkillProperty( cur->entity_skill_id );
			if ( !_stricmp( prop->name, name.c_str( ) ) )
				return cur;
		}

		return nullptr;
	}

	EntitySkill * choose_best_combat_skill( Entity * pLocal, Entity * pTarget )
	{
		// first off, get our skills.
		std::vector<EntitySkill *> skills;

		for ( int i = 0; i < _countof( pLocal->pSkills ); i++ )
		{
			auto cur = &pLocal->pSkills[ i ];
			if ( cur->entity_skill_id == -1 )
				break;

			if ( Helper::CanCastSkill( pLocal, cur ) )
				skills.push_back( cur );
		}

		if ( skills.size( ) )
		{
			EntitySkill * chosen = nullptr;
			int highest_level = 0;
			auto mob = pGNames->GetMobProperty( pTarget->object_id );

			for ( auto & x : skills )
			{
				auto skill = pGNames->GetSkillProperty( x->entity_skill_id );
				if ( skill->IsAttackSkill( ) == false )
					continue;

				bool hardCounter = DoesElementCounter( skill->skill_element, mob->mob_element );

				// greater than max OR counter-2 levels, AND not countered, AND not same
				if ( ( x->points_given > highest_level || ( hardCounter && x->points_given >= ( highest_level - 2 ) ) )
					 && ( ( DoesElementCounter( mob->mob_element, skill->skill_element ) == false && mob->mob_element != skill->skill_element ) || x->points_given > ( highest_level + 5 ) )
					 )
				{
					chosen = x; // dont update highest level then.
					auto new_level = hardCounter ? x->points_given + 2 : x->points_given;
					highest_level = new_level;
				}
			}

			return chosen;
		}
		else
			return nullptr;
	}

	void do_autoattack( Entity * pTarget )
	{
		auto pLocal = Helper::GetLocalPlayer( );
		memset( &pLocal->pAttack.skill_attack, 0, sizeof( pLocal->pAttack.skill_attack ) );
		Helper::FollowEntity( pTarget );
		Helper::SetTarget( pTarget );
		pLocal->pAttack.attack_type = EntityAttack::AttackType::Auto;
		pLocal->pAttack.autoattack.target_id = pTarget->entity_id;
	}

	void stop_attacking( )
	{
		auto pLocal = Helper::GetLocalPlayer( );
		pLocal->pAttack.attack_type = EntityAttack::AttackType::None;
		pLocal->pAttack.skill_attack.current_attack = EntityAttack::AttackType::None;
		pLocal->target_entity_id = -1;
		Helper::SetTarget( 0 );
		Helper::FollowEntity( nullptr );
	}

	void MoveForward( Entity * pLocal )
	{
		pLocal->pPlayerInput->translation_flags = PlayerInput::Translation::forward;
	}

	void SpinCharacter( Entity * pLocal, bool left )
	{
		pLocal->pPlayerInput->rotation_flags = left ? PlayerInput::Rotation::left : PlayerInput::Rotation::right;
	}

	void MoveBackward( Entity * pLocal )
	{
		pLocal->pPlayerInput->translation_flags = PlayerInput::Translation::back;
	}

	void Jump( Entity * pLocal )
	{
		pLocal->pPlayerInput->rotation_flags = PlayerInput::Rotation::jump;
	}

	bool IsPlayerOccupied( )
	{
		auto pLocal = Helper::GetLocalPlayer( );

		return( Globals::IsPlayerPathingToQuest( ) || Globals::player_state == PlayerState::resting || Globals::player_state == PlayerState::fetching );
	}

	InventoryItem * GetInventoryItem( Entity * pLocal, int item_id )
	{
		for ( unsigned int x = 0; x < pLocal->pInventoryInfo.max_player_slots; x++ )
		{
			auto cur = pLocal->pInventoryInfo.pInventory->pItems[ x ];
			if ( cur.object_id == item_id )
				return &cur;
		}

		return nullptr;
	}

	MapManager * GetMapManagerForMapID( int id )
	{
		return pMapPtr->GetMapManager( id );
	}

	MapObjects * FindMapObjectForItemID( int item_id )
	{
		for ( int i = 5; i < 0xC; i++ )
		{
			auto map_man = Helper::GetMapManagerForMapID( i );
			if ( map_man == nullptr )
				continue;

			for ( int x = 0; x < map_man->max_entries; x++ )
			{
				auto curObj = map_man->objects[ x ];
				auto prop = pGNames->GetItemPropertyForID( curObj->item_id_dropped );
				if ( !prop )
					continue;

				//LogLn( "SEE ITEM %s", prop->item_name );
				if ( curObj->item_id_dropped == item_id )
					return curObj;
			}
		}

		return nullptr;
	}

	bool HasFlyingVehicle( )
	{
		auto pLocal = Helper::GetLocalPlayer( );
		for ( int i = 0; i < pLocal->pInventoryInfo.max_player_slots; i++ )
		{
			auto cur_item = &pLocal->pInventoryInfo.pInventory->pItems[ i ];
			if ( cur_item->object_id == 0 )
				continue;
			auto prop = pGNames->GetItemPropertyForID( cur_item->object_id );
			if ( prop == nullptr )
				continue;

			if ( strstr( prop->item_name, "Board" ) || strstr( prop->item_name, "Broom" ) )
				return true;
			else
				continue;
		}

		return false;
	}

	void EquipFlyingVehicle( bool mount )
	{
		auto pLocal = Helper::GetLocalPlayer( );
		if ( pLocal->is_channeling_item != false )
			return; // already doing it brah.

		for ( int i = 0; i < pLocal->pInventoryInfo.max_player_slots; i++ )
		{
			auto cur_item = &pLocal->pInventoryInfo.pInventory->pItems[ i ];
			if ( cur_item->object_id == 0 )
				continue;
			auto prop = pGNames->GetItemPropertyForID( cur_item->object_id );
			if ( prop == nullptr )
				continue;

			if ( strstr( prop->item_name, "Board" ) || strstr( prop->item_name, "Broom" ) )
			{
				if ( mount )
				{
					// check one more time.
					if ( Input::IsOnGround( ) )
						cur_item->UseItem( );
					else
						pLocal->is_channeling_item = 1;
				}
				else
				{
					if ( Input::IsOnGround( ) == false ) // for dismount.
						cur_item->UseItem( );
				}

				return;
			}
			else
				continue;
		}
	}

	void ResetAFK( bool down )
	{
		PostMessageA( Globals::hwndFlyff, down ? WM_KEYDOWN : WM_KEYUP, VK_SPACE, 0 );
	}

	bool CastRay( vec3 * hit_pos, vec3 * start, vec3 * end, int param4, int param5, int param6 )
	{
		//b8 34 9d 0 0
		using cr_t = bool( __stdcall * )( vec3 *, vec3 *, vec3 *, int, int, int );
		static cr_t fn = nullptr;
		if ( fn == nullptr )
		{
			auto ret = Utility::FindPattern( GetModuleHandleA( 0 ), "b8 34 9d 0 0" );
			ret -= 3;
			fn = ( decltype( fn ) )( ret );
		}

		return fn( hit_pos, start, end, param4, param5, param6 );
	}

	// hmm.... do we want highest? or lowest (cheapest?)
	InventoryItem * FindHealingItem( Entity * pLocal, heal_type ht, search_type st )
	{
		InventoryItem * best_heal = nullptr;
		int best_heal_num = st == search_type::strongest ? 0 : INT_MAX;

		for ( int i = 0; i < pLocal->pInventoryInfo.max_player_slots; i++ )
		{
			auto cur_item = &pLocal->pInventoryInfo.pInventory->pItems[ i ];
			if ( cur_item->object_id == 0 )
				continue;

			auto prop = pGNames->GetItemPropertyForID( cur_item->object_id );

			if ( prop->heal_amount == -1 ) // cant heal
				continue;

			if ( ht == heal_type::hp )
			{
				if ( prop->item_type_one != ItemProperty::ItemType::food )
					continue;
			}
			else if ( ht == heal_type::mp )
			{
				if ( prop->item_type_one != ItemProperty::ItemType::mana_potion )
					continue;
			}
			else
			{
				if ( prop->item_type_one != ItemProperty::ItemType::vita_drink )
					continue;
			}

			if ( st == search_type::strongest )
			{
				if ( prop->heal_amount < best_heal_num )
					continue;

				best_heal_num = prop->heal_amount;
				best_heal = cur_item;
			}
			else
			{
				// cheapest (weakest)
				if ( prop->heal_amount > best_heal_num )
					continue;

				best_heal_num = prop->heal_amount;
				best_heal = cur_item;
			}
		}

		return best_heal;
	}

	int GetItemIDForName( std::string item_name )
	{
		auto item_list = pGNames->items;

		for ( int i = 0; i < item_list.entries; i++ )
		{
			auto cur_item = ( ItemProperty * )item_list.pData[ i ];
			if ( !cur_item || cur_item->object_id == 0 )
				continue;

			if ( !_stricmp( cur_item->item_name, item_name.c_str( ) ) )
				return cur_item->object_id;
		}

		return -1;
	}

	int GetMobIDForName( std::string mob_name )
	{
		for ( int i = 0; i < pGNames->max_mobs; i++ )
		{
			auto cur_mob = &pGNames->mobs[ i ];
			if ( cur_mob->object_id == 0 )
				continue;

			if ( !_stricmp( cur_mob->name, mob_name.c_str( ) ) )
				return cur_mob->object_id;
		}

		return -1;
	}
}