#include "wings_core.hpp"
#include <string>
#include "cmd_handler.hpp"

FILE * con = nullptr;
struct data
{
	HMODULE me;
	HANDLE hThread;
};

void flush_stdin( )
{
	DWORD dwTmp;
	INPUT_RECORD ir[ 2 ] = { 0 };
	ir[ 0 ].EventType = KEY_EVENT;
	ir[ 0 ].Event.KeyEvent.bKeyDown = TRUE;
	ir[ 0 ].Event.KeyEvent.dwControlKeyState = 0;
	ir[ 0 ].Event.KeyEvent.uChar.UnicodeChar = VK_RETURN;
	ir[ 0 ].Event.KeyEvent.wRepeatCount = 1;
	ir[ 0 ].Event.KeyEvent.wVirtualKeyCode = VK_RETURN;
	ir[ 0 ].Event.KeyEvent.wVirtualScanCode = MapVirtualKey( VK_RETURN, MAPVK_VK_TO_VSC );
	ir[ 1 ] = ir[ 0 ];
	ir[ 1 ].Event.KeyEvent.bKeyDown = FALSE;
	WriteConsoleInput( GetStdHandle( STD_INPUT_HANDLE ), ir, 2, &dwTmp );
}

BOOL __stdcall GetFlyff( HWND h, LPARAM l)
{
	char buf[ 120 ] = { 0 };
	GetWindowTextA( h, buf, 120 );

	if ( strstr( buf, "Flyff" ) )
	{
		Globals::hwndFlyff = h;
	}

	return true;
}

DWORD __stdcall InputThread( LPVOID )
{
	while ( !GetAsyncKeyState( VK_NUMPAD8 ) )
	{
		std::string input;
		std::getline( std::cin, input );
		if ( input == "exit" || input == "\n" )
			break;
		Input::parse_input( input );
		Sleep( 5 );
	}
	return 0;
}

DWORD __stdcall MainThread( LPVOID mod )
{
	EnumWindows( GetFlyff, 0 );

	LogLn<green>( "Locked and loaded. Press NUMPAD7 to toggle farmer. Press NUMPAD8 to terminate." );
	bool paused = true;
	data * pData = ( data * )mod;

	while ( !GetAsyncKeyState( VK_NUMPAD8 ) )
	{
		if ( GetAsyncKeyState( VK_NUMPAD7 ) & 1 )
		{
			paused = !paused;
			LogLn<cyan>( "Paused: %d", paused );
		}

		if ( !paused )
			Wings::main_loop( );

		Sleep( 5 );
	}

	LogLn<red>( "Terminating input thread." );

	flush_stdin( );

	//TerminateThread( pData->hThread, 0 );
	WaitForSingleObject( pData->hThread, INFINITE );

	LogLn<red>( "Goodbye." );
	fclose( stdin );
	fclose( stdout );
	SetConsoleTitleA( "Inactive." );
	FreeConsole( );
	FreeLibraryAndExitThread( pData->me, 0 );
}

BOOL WINAPI DllMain( HMODULE me, DWORD dwReason, LPVOID )
{
	if ( dwReason == DLL_PROCESS_ATTACH )
	{
		AllocConsole( );
		if ( freopen_s( &con, "CONOUT$", "w", stdout ) == 0 )
		{
			freopen_s( &con, "CONIN$", "r", stdin );
			SetConsoleTitleA( "Flying is cool" );

			HANDLE hThread = CreateThread( 0, 0, InputThread, 0, 0, 0 );
			auto to_Send = new data{ me, hThread };
			CloseHandle( CreateThread( 0, 0, MainThread, to_Send, 0, 0 ) );
		}
		else
			MessageBoxA( 0, "No Console.", ":(", 0 );
	}

	return 1;
}

// TODO !!Important now.: 3rd column skills > 2nd > 1st, how do we differentiate so we choose our actual highest damager?. Maybe just let playaer choose. fuck it.
// TODO: Ignore dropped items that are specific for questing unless we have that quest. description says quest item
// TODO: Tether to quest location for easier gathering of mobs.
// NOTE: Maybe kiting.
// TODO: Boss avoidance.
// TODO: Scan for aggro mobs while resting and preemptively fight them.
// If u wanna be pro:
// TODO: Check our buffs, see if we have any buff skills, cast when they run out (and resting)
// TODO: NOTE: We cant get map info until we open map and toggle enemies at least once.