#include "AssistHandler.hpp"
#include "wings_helper.hpp"

namespace Assist
{
	bool HandleRest( )
	{
		auto pLocal = Helper::GetLocalPlayer( );
		// Do buffs during rest.

		auto maxhp = pLocal->GetMaxHP( );
		auto maxfp = pLocal->GetMaxFP( );
		auto maxmp = pLocal->GetMaxMP( );

		auto ready = ( pLocal->hp >= maxhp && pLocal->fp >= maxfp && pLocal->mp >= 50.f ); // 50 to cast stonehand
		auto has_buff = pLocal->HasBuff( "stonehand" );

		if ( ready )
		{
			auto cast_buff = has_buff ? ( pLocal->elapsed_time - has_buff->buff_cast_time ) >= ( has_buff->buff_max_duration / 2.f ) : true;

			if ( cast_buff )
			{
				// attempt to cast stonefist ONLY when we are good to go otherwise
				auto stunbuff = Helper::find_skill_in_entity( pLocal, "stonehand" );
				if ( stunbuff && stunbuff->points_given )
				{
					Helper::cast_skill( pLocal, stunbuff );
					Sleep( 10 );
				}
			}
		}

		// everything over half.
		return ready;
	}

	void HandleCombat( )
	{
		auto pLocal = Helper::GetLocalPlayer( );

		auto skill = Helper::choose_best_combat_skill( pLocal, Globals::farming_target );
		// HP Recovers faster, therefore id rather just auto attack people if our STR/STA/DEX is high enough...
		// plus we get the knockback and stun chance
		// Check if we are buffed with StoneFist. If so: we autoattack
		// If not, we skill?

		auto normal_attack = [ &pLocal ]( )
		{
			// Default to an auto attack
			if ( pLocal->pos.distance( Globals::farming_target->pos ) <= 1.f )
				Globals::SetPlayerState( PlayerState::attacking );

			Helper::do_autoattack( Globals::farming_target );
		};

		if ( pLocal->on_hit_buff == 12 )
			normal_attack( );
		else
		{
			if ( skill )
			{
				if ( pLocal->can_cast_skill && !pLocal->is_casting_skill )
					Helper::cast_skill( Globals::farming_target, skill ); // I think what happens after this is that we need to reset the state.
			}
			else
				normal_attack( );
		}
	}

}