#include "CombatHandler.hpp"
#include "PathingHandler.hpp"
#include "wings_helper.hpp"
#include "QuestHandler.hpp"
#include "AssistHandler.hpp"


namespace Combat
{
	void use_necessary_potions( )
	{
		auto pLocal = Helper::GetLocalPlayer( );
		auto max_hp = pLocal->GetMaxHP( );
		auto max_mp = pLocal->GetMaxMP( );
		auto max_fp = pLocal->GetMaxFP( );
		auto hp_diff = max_hp - pLocal->hp;
		auto mp_diff = max_mp - pLocal->mp;
		auto fp_diff = max_fp - pLocal->fp;

		auto pMob = pGNames->GetMobProperty( Globals::farming_target->object_id );


		// NOTE: damage appears to be something like (dmg_hi - (defense*diminishing_return))
				// dmg lo is good because sometimes they get quite close to that and that num can be quite high.

		auto crit_max = pMob->dmg_max * 1.49864498644986449864; // im not dying to a crit god dammit
		if ( pMob->mob_level <= pLocal->entity_level )
			crit_max = pMob->dmg_max / 2; // we get that juicy overlevel defense. -- wheeew, maybe too close?

		// ASDF: Account for defense? 0.7263681592039800995 diminishing returns?. Final answer +- 10%
		// dmgLo - defense*diminish - 10%
		// i think crits ignore defense. otherwise we can calculate a "normal" hit

		if ( /*( pLocal->GetHP( ) / max_hp ) <= .5f ||*/ pLocal->hp <= ( crit_max * 1.5f ) ) // little bit less than 2x just to be wary.
		{
			auto item = Helper::FindHealingItem( pLocal, Helper::heal_type::hp, Helper::search_type::cheapest );
			if ( item )
			{
				auto prop = pGNames->GetItemPropertyForID( item->object_id );
				// can heal for <= how much we are missing, so we arent wasting.
				if ( prop->heal_amount <= hp_diff )
					item->UseItem( );
			}
		}

		if ( ( pLocal->GetMP( ) / max_mp ) <= .1f && ( Globals::mp_potion_timer - std::chrono::system_clock::now( ) ).count( ) <= 0.f )
		{
			auto item = Helper::FindHealingItem( pLocal, Helper::heal_type::mp, Helper::search_type::cheapest );
			if ( item )
			{
				auto prop = pGNames->GetItemPropertyForID( item->object_id );
				if ( prop->heal_amount <= mp_diff )
				{
					item->UseItem( );
					Globals::mp_potion_timer = std::chrono::system_clock::now( ) + std::chrono::seconds( 2 ); // wait 2 second between MP potion uses so we dont spam.
				}
			}
		}

		auto need_potion = ( pLocal->fp <= 30 ); // this is the most we'll ever need.

		if ( need_potion && ( Globals::fp_potion_timer - std::chrono::system_clock::now( ) ).count( ) <= 0.f )
		{
			auto item = Helper::FindHealingItem( pLocal, Helper::heal_type::fp, Helper::search_type::cheapest );
			if ( item )
			{
				auto prop = pGNames->GetItemPropertyForID( item->object_id );
				if ( prop->heal_amount <= fp_diff )
				{
					item->UseItem( );
					Globals::fp_potion_timer = std::chrono::system_clock::now( ) + std::chrono::seconds( 2 ); // wait 2 second between FP potion uses so we dont spam.
				}
			}
		}
	}

	farm_ret AcquireTarget( bool aggro_override )
	{
		auto pLocal = Helper::GetLocalPlayer( );
		// Only target entities within 2/3 levels of us.
		auto level_lambda = [ ]( Entity * curEnt, Entity * to ) -> bool
		{
			return ( curEnt->entity_level >= ( to->entity_level - Globals::target_mob_levels.first ) && curEnt->entity_level <= ( to->entity_level + Globals::target_mob_levels.second ) );
		};

		auto certain_mob_lambda = [ ]( Entity * curEnt, Entity * to ) -> bool
		{
			for ( unsigned int i = 0; i < Globals::target_mob_type.size( ); i++ )
			{
				auto & x = Globals::target_mob_type[ i ];
				if ( x.second == 0xDEADBABE )
					return false;

				if ( x.first == curEnt->object_id )
				{
					if ( x.second == -1 )
						return true;

					return true;
				}
			}

			return false;
		};

		if ( aggro_override )
		{
			auto aggro_lambda = [ ]( Entity * curEnt, Entity * to ) -> bool
			{
				return ( curEnt->pAttack.attack_type == EntityAttack::AttackType::MobAttack && curEnt->pAttack.autoattack.target_id == to->entity_id );
			};

			Globals::farming_target = Helper::FindClosestMob( aggro_lambda );
		}
		else
		{
			if ( Globals::target_mob_type.size( ) != 0 )
			{
				if ( Globals::target_mob_type.front( ).first == 0xCAFEBABE )
				{
					// then attack ANYONE nearby. (why? idk... area farming? for an item? idk.)
					Globals::farming_target = Helper::FindClosestMob( ); // careful, they can be high level.
				}
				else
					Globals::farming_target = Helper::FindClosestMob( certain_mob_lambda );
			}
			else
				Globals::farming_target = Helper::FindClosestMob( level_lambda );
		}

		if ( Globals::farming_target == nullptr )
		{
			Globals::fail_count++;
			if ( Globals::fail_count > 10 )
				return farm_ret::exhausted;
			else
				return farm_ret::none;
		}
		else
		{
			for ( unsigned int i = 0; i < Globals::target_mob_type.size( ); i++ )
			{
				auto & x = Globals::target_mob_type[ i ];

				if ( x.first == Globals::farming_target->object_id )
				{
					if ( x.second == -1 )
						break;

					if ( x.second-- == 0 )
					{
						LogLn<bright_blue>( "Finished farming mob %d", Globals::farming_target->object_id );
						Globals::target_mob_type.erase( Globals::target_mob_type.begin( ) + i, Globals::target_mob_type.begin( ) + i + 1 );
						// path to new location if necessary
						if ( Quest::IsQuesting( ) )
							Pathing::PathToQuest( Quest::GetCurrentQuestProperty( ) );
					}
					else
						LogLn<bright_blue>( "Farming mob %d, %d times remaining", Globals::farming_target->object_id, x.second );

					break;
				}
			}

			Globals::SetPlayerState( PlayerState::pathing );
			Globals::fail_count = 0;
			LogLn<green>( "Targetting %s [%p / %X]", Globals::farming_target->entity_name, Globals::farming_target, Globals::farming_target->entity_id );
			if ( Helper::GetTarget( pLocal ) )
			{
				pLocal->pEntityTarget->pTargetEntity = Globals::farming_target;
				Helper::FollowEntity( Globals::farming_target );
				Helper::ResetAFK( );
			}
		}

		return farm_ret::engaged;
	}

	bool check_attack_bugged( Entity * pLocal, Entity * pTarget )
	{
		static std::chrono::system_clock::time_point attack_timer;
		static Entity * last_ent = nullptr;
		static int last_hp = 0;
		auto difference = std::chrono::duration_cast< std::chrono::seconds >( ( std::chrono::system_clock::now( ) - attack_timer ) );

		if ( last_ent != pTarget )
		{
			last_ent = pTarget;
			last_hp = pTarget->hp;
			attack_timer = std::chrono::system_clock::now( );
		}
		else if ( difference.count( ) >= .5 )
		{
			attack_timer = std::chrono::system_clock::now( );
			// TODO: do we need this to be bigger? i dont think skills get hit by this shit. (attack bugs, that is)
			if ( pLocal->pos.distance( pTarget->pos ) > 5.f )
				return false;

			last_ent = nullptr;
			// Check if mob aggroed to us while we have them in current attack. If not, we must be bugged.
			return ( pTarget->mob_target_id != pLocal->entity_id && last_hp == pTarget->hp );
		}

		return false; // default false.
	}

	farm_ret seek_and_kill_mobs( bool aggro_override )
	{
		// Get new target
		while ( Globals::farming_target == nullptr )
		{
			Helper::ResetAFK( );

			if ( Combat::AcquireTarget( aggro_override ) == farm_ret::exhausted )
				return farm_ret::exhausted;
		}

		Helper::ResetAFK( false );

		return Combat::HandleCombat( );
	}

	farm_ret HandleCombat( )
	{
		auto pLocal = Helper::GetLocalPlayer( );

		// Out here so we dont die lol.
		Combat::use_necessary_potions( );

		auto reset_farming = [ & ]( )->void
		{
			Globals::farming_target = nullptr;
			pLocal->pEntityTarget->pTargetEntity = nullptr;
			pLocal->pAttack.attack_type = EntityAttack::AttackType::None;
			Globals::stuck_count = 0;
		};

		if ( Globals::stuck_count >= 50 )
		{
			// TODO: Maybe check our target hasnt focused us so we're not double aggroing?
			LogLn<bright_red>( "Idiot bot is stuck. Changing targets." );
			reset_farming( );
		}

		// When we are currently bugged and not hitting anyone
		if ( Globals::paused )
		{
			if ( ( Globals::attack_pause_timer - std::chrono::system_clock::now( ) ).count( ) <= 0 )
			{
				//Helper::ResetAFK( false );

				//Helper::do_autoattack( Globals::farming_target );
				// passed the timer.
				Globals::paused = false;
			}
			else
			{
				//Helper::Jump( pLocal );
				pLocal->pPlayerInput->attack_input = 0;
			}
		}
		else
		{
			if ( !Globals::farming_target )
				return farm_ret::none;

			if ( Globals::farming_target->IsAlive( ) == false )
			{
				LogLn<gold>( "We killed a level %d %s", Globals::farming_target->entity_level, Globals::farming_target->entity_name );
				LogLn<gold>( "We've killed %d mobs.", ++Globals::kill_count );
				reset_farming( );
				return farm_ret::kill;
			}
			else if ( !Globals::farming_target->IsValid( ) )
			{
				LogLn( "Entity %p (%s) no longer valid.", Globals::farming_target, Globals::farming_target->entity_name );
				reset_farming( );
				return farm_ret::kill;
			}
			else
			{
				auto no_skill_block = ( pLocal->is_casting_skill == false && pLocal->can_cast_skill );

				if ( ( Globals::move_check_timer - std::chrono::system_clock::now( ) ).count( ) <= 0 )
				{
					Globals::move_check_timer = std::chrono::system_clock::now( ) + std::chrono::milliseconds( 250 );
					if ( Pathing::handle_stuck_pathing( ) )
					{
						Globals::attack_pause_timer = std::chrono::system_clock::now( ) + std::chrono::milliseconds( 100 );// wait 2 seconds before next attempt.
						Globals::paused = true;
					}
				}

				if ( check_attack_bugged( pLocal, Globals::farming_target ) && pLocal->is_casting_skill == false )
				{
					Helper::stop_attacking( );
					Helper::ResetAFK( );
					Sleep( 100 );
					Helper::ResetAFK( false );
					Helper::do_autoattack( Globals::farming_target );
				}

				// use skills until we are out of MP/FP
				if ( pLocal->entity_job == Entity::EntityJob::assist )
					Assist::HandleCombat( );
				else
				{
					auto skill = Helper::choose_best_combat_skill( pLocal, Globals::farming_target );
					if ( skill )
					{
						if ( pLocal->can_cast_skill && !pLocal->is_casting_skill )
							Helper::cast_skill( Globals::farming_target, skill ); // I think what happens after this is that we need to reset the state.
					}
					else
					{
						// Default to an auto attack
						if ( pLocal->pos.distance( Globals::farming_target->pos ) <= 1.f )
							Globals::SetPlayerState( PlayerState::attacking );

						Helper::do_autoattack( Globals::farming_target );
					}
				}
			}
		}

		return farm_ret::engaged;
	}
}