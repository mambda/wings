#pragma once
#include "CombatHandler.hpp"

namespace Assist
{
	bool HandleRest( );
	void HandleCombat( );
}