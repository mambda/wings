#include "wings_globals.hpp"

namespace Quest
{
	bool HasQuest( );

	EntityQuest * GetNextQuest( );

	void SetQuest( EntityQuest * newQuest );

	bool IsQuestCompleted( EntityQuest * pQuest = nullptr );

	QuestProperty * GetCurrentQuestProperty( );
	void ClearQuest( );
	bool IsQuesting( );

	EntityQuest * GetCurrentQuest( );
	vec3 GetDestination( );
}