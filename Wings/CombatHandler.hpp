#pragma once
#include "wings_globals.hpp"

enum class farm_ret
{
	none,
	engaged,
	kill,
	exhausted,
};

namespace Combat
{
	void use_necessary_potions( );
	farm_ret AcquireTarget( bool aggro_override );
	farm_ret seek_and_kill_mobs( bool aggro_override = false );
	farm_ret HandleCombat( );
}