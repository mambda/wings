#include "InputHandler.hpp"

namespace Input
{
	bool IsOnGround( )
	{
		auto pLocal = Helper::GetLocalPlayer( );
		return ( HasMovementState( PlayerInput::Movement::running ) || HasMovementState( PlayerInput::Movement::walking ) );
	}

	bool HasMovementState( PlayerInput::Movement state )
	{
		auto pLocal = Helper::GetLocalPlayer( );
		return ( pLocal->pPlayerInput->move_state == state );
	}

	void SetMovementState( PlayerInput::Movement state )
	{
		auto pLocal = Helper::GetLocalPlayer( );
		pLocal->pPlayerInput->move_state = state;
		return;
	}

	void SetRotationFlag( PlayerInput::Rotation flag )
	{
		auto pLocal = Helper::GetLocalPlayer( );
		pLocal->pPlayerInput->rotation_flags |= flag;
		return;
	}

	void SetTranslationState( PlayerInput::Translation state )
	{
		auto pLocal = Helper::GetLocalPlayer( );
		pLocal->pPlayerInput->translation_flags = state;
		return;
	}
}