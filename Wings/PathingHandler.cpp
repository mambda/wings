#include "wings_globals.hpp"
#include "PathingHandler.hpp"
#include "QuestHandler.hpp"
#include <corecrt_math_defines.h>
#include "InputHandler.hpp"

namespace Pathing
{
	void RotateYawToPoint( vec3 point )
	{
		auto pLocal = Helper::GetLocalPlayer( );
		//pLocal->pitch = 0;
		auto delta = point - pLocal->pos;
		auto dist = pLocal->pos.distance2d( point );
		auto rads = atanf( delta.z / delta.x );
		rads /= M_PI;
		auto new_rotation = ( rads * 180.f )/* + 90.f*/; // SHEEEEEEESH???? BECAUSE 0 == SOUTH? INSTEAD OF STRIGHT? IDK LOL.

		// quadrants. bitch.
		if ( delta.x < 0.f )
			new_rotation -= 90.f;
		else
			new_rotation += 90.f; // this is a TANGENT THING

		pLocal->yaw = new_rotation; // PRAYGE
	}

	void MoveRandom( int radius )
	{
		auto pLocal = Helper::GetLocalPlayer( );
		pLocal->mouse_target_dest = pLocal->pos + vec3( rand( ) % radius, 0.f, rand( ) % radius ); // move to a random point near the hit destination.
	}

	bool handle_stuck_pathing( )
	{
		auto pLocal = Helper::GetLocalPlayer( );
		auto hasTarget = Helper::IsFollowingTarget( pLocal );
		if ( !hasTarget && !Globals::IsPlayerPathingToQuest( ) )/*&& Globals::IsPlayerPathingToQuest( ) == false && Globals::player_state != PlayerState::attacking )*/
			return false;

		// not on the ground OR channeling an item.
		if ( ( !Input::HasMovementState( PlayerInput::Movement::walking ) && !Input::HasMovementState( PlayerInput::Movement::running ) ) || pLocal->is_channeling_item )
			return false;

		vec3 out, start, end;
		start = pLocal->pos;
		if ( hasTarget )
		{
			auto target = Helper::GetTarget( pLocal );
			if ( !target )
				return false;
			end = target->pos;
			auto ret = Helper::CastRay( &out, &start, &end );

			if ( ret )
			{
				Globals::stuck_count++;
				LogLn( "Cast ray stuck!" );
				pLocal->mouse_target_dest = out + vec3( ( 3 + rand( ) % 10 ), 0.f, 3 + ( rand( ) % 10 ) ); // move to a random point near the hit destination.
				if ( Globals::stuck_count % 10 == 0 )
					Helper::Jump( pLocal );
			}

			return ret;
		}
		else if ( Globals::IsPlayerPathingToQuest( ) ) // arbitrary 10 units.
		{
			auto quest = Globals::target_quest;
			end = Quest::GetDestination( );
			auto ret = Helper::CastRay( &out, &start, &end );

			if ( ret )
			{
				auto target_pos = out + vec3( ( 3 + rand( ) % 10 ), 0.f, 3 + ( rand( ) % 10 ) );
				pLocal->mouse_target_dest = target_pos;
				Helper::Jump( pLocal );
			}

			return ret;
		}

		return false;
	}

	void check_jump_required( Entity * pLocal, Entity * pTarget )
	{
		auto y_dist = abs( pTarget->pos.y - pLocal->pos.y );
		vec3 out, start = pLocal->pos + vec3( 0.f, 2.5f, 0.f ), end = pTarget->pos;
		auto ret = Helper::CastRay( &out, &start, &end );
		if ( !ret )
		{
			// they are visible if we jump. then JUMP
		}

		// TODO: Jump if target Y higher than us.
	}

	bool check_reached_quest( )
	{
		auto pLocal = Helper::GetLocalPlayer( );

		vec3 target_dest = Quest::GetDestination( );
		// Maybe dont be lazy and imagine all target mobs are in the same place.


		if ( pLocal->pos.distance2d( target_dest ) <= 5.f )
		{
			LogLn<gold>( "Arrived at quest destination!" );
			return true;
		}
		return false;
	}

	void Reset( )
	{
		if ( !Input::IsOnGround( ) )
		{
			Input::SetMovementState( PlayerInput::Movement::hovering );
			Helper::EquipFlyingVehicle( false ); // should remove it, right?
			Helper::ResetAFK( );
		}

		auto pLocal = Helper::GetLocalPlayer( );
		Globals::SetPlayerState( PlayerState::none );
		pLocal->mouse_target_dest.zero( );
	}

	void FlyToPoint( vec3 point )
	{
		if ( point.is_zero( ) )
			return;

		auto pLocal = Helper::GetLocalPlayer( );
		pLocal->new_button_press = -1;

		Pathing::RotateYawToPoint( point );

		//Input::SetRotationFlag( PlayerInput::Rotation::up );
		if ( pLocal->pos.y < 190.f )
			pLocal->pitch = -44.f; // str8 up
		else
			pLocal->pitch = 0.f;

		Input::SetMovementState( PlayerInput::Movement::fly );
		Input::SetTranslationState( PlayerInput::Translation::forward );
	}

	void FlyToQuest( QuestProperty * pQuest )
	{
		if ( Input::IsOnGround( ) )
			Helper::EquipFlyingVehicle( );
		else
			FlyToPoint( Quest::GetDestination( ) );
	}

	void WalkToQuest( QuestProperty * pQuest )
	{
		auto pLocal = Helper::GetLocalPlayer( );
		vec3 target_dest = Quest::GetDestination( );

		if ( target_dest.is_zero( ) )
			return;
		pLocal->mouse_target_dest = target_dest;//might need to fib the Y pos
	}

	void PathToQuest( QuestProperty * pQuest )
	{
		// TODO: check if in no fly zone.
		if ( Helper::HasFlyingVehicle( ) )
			Pathing::FlyToQuest( pQuest );
		else
			Pathing::WalkToQuest( pQuest );
	}
}