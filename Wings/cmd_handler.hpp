#include "wings_core.hpp"
#include "wings_helper.hpp"

namespace Input
{
	void parse_input( std::string input )
	{
		using namespace Wings;

		if ( !Globals::initialized )
			Wings::initialize( );

		const std::initializer_list<const std::pair<const std::string, const std::string>> descriptions =
		{
			{"clear", "Removes any restraints on monster farming." },
			{"stop", "Disengages from current monster target." },
			{"attack", "To attack certain monsters. Usages:\n\t" "attack <mob1_name>,<mob2_name>\n\t" "attack lower_level_difference,upper_level_difference\n\t" "attack * (to attack any monster nearby)" },
			{"focus", "To only pickup certain items. Usage: focus item,names,here"},
			{"ignore", "To ignore certain items frmo pickup. Usage: ignore item,names,here"},
			{"reset_items", "To reset item pickup rules"},
			{"query", "Check for quantity of item in inventory. Usage: query item_name"},
			{"start_quest", "Go to all quest areas and collect the necessary items."},
			{"stop_quest", "Stop questing."},
		};


		if ( input.size( ) != 0 )
		{
			// attack name,name,name
			// attack level_diff_lo, level_diff_hi

			if ( input == "clear" )
			{
				Globals::target_mob_type.clear( );
				Globals::target_mob_levels = { 2,1 };
				Globals::farming_target = nullptr;
				Globals::exhaust_pause = false;
				LogLn<bright_blue>( "Resuming normal farming." );
				return;
			}
			else if ( input == "start_quest" )
			{
				Globals::questing = true;
				Globals::target_quest = nullptr;
				Globals::SetPlayerState( PlayerState::none );
				return;
			}
			else if ( input == "stop_quest" )
			{
				Globals::questing = false;
				Globals::target_quest = nullptr;
				Globals::SetPlayerState( PlayerState::none );
				return;
			}
			else if ( input == "stop" )
			{
				Globals::farming_target = nullptr;
				LogLn<bright_blue>( "Clearing target" );
				return;
			}
			else if ( input == "reset_items" )
			{
				Globals::desired_items.clear( );
				Globals::ignore_items = false;
				LogLn<gold>( "Back to default item gathering." );
				return;
			}
			else if ( input == "help" )
			{
				for ( auto & p : descriptions )
				{
					Log<bright_green>( ( p.first + " - " ).c_str( ) );
					LogLn<gold>( p.second.c_str( ) );
				}
				return;
			}
			else
			{
				// Parse.
				std::istringstream iss( input );
				std::vector<std::string> tokens;
				std::string token;
				iss.seekg( input.find( ' ' ) + 1 );

				while ( std::getline( iss, token, ',' ) )
				{
					if ( token.front( ) == ' ' )
						token.erase( 0, 1 );
					tokens.push_back( token );
				}

				if ( strstr( input.c_str( ), "attack" ) )
				{
					Globals::SetPlayerState( PlayerState::none );
					if ( Globals::questing )
					{
						Globals::questing = false;
						LogLn<yellow>( "Stopping questing to focus attack." );
					}

					if ( tokens.size( ) < 1 )
					{
						LogLn<bright_red>( "Invalid syntax. Either attack <names>:optional_count,<of>,<mobs>,<base_name*>(attack all).\nOr attack lower_level_diff,higher_level_diff" );
						return;
					}

					Globals::target_mob_type.clear( );

					auto ch = *tokens[ 0 ].c_str( );
					if ( isalpha( ch ) )
					{
						// doing names
						for ( unsigned int i = 0; i < tokens.size( ); i++ )
						{
							auto & tok = tokens[ i ];

							std::istringstream amt( tok );
							std::string amount;
							std::getline( amt, amount, ':' );
							std::getline( amt, amount, ':' ); // wouldbe the second one.
							int kill_amount = atol( amount.c_str( ) );
							if ( kill_amount == 0 )
								kill_amount = -1;

							auto pos = tok.find( ':' );
							if ( pos != tok.npos )
								tok = tok.substr( 0, pos );

							if ( tok.back( ) == '*' )
							{
								tok.pop_back( );
								std::string others[ ] = { "small ", "captain ", "", }; // there are other variations but this is the most simple.
								for ( int x = 0; x < ( sizeof( others ) / sizeof( std::string ) ); x++ )
								{
									auto full_str = std::string( others[ x ] + tok );
									LogLn( "Searching for mob %s", full_str.c_str( ) );
									auto mob_id = Helper::GetMobIDForName( full_str.c_str( ) );
									if ( mob_id == -1 )
									{
										LogLn<bright_red>( "%s invalid mob name.", full_str.c_str( ) );
										continue;
									}

									Globals::target_mob_type.push_back( { mob_id, kill_amount } );
									if ( kill_amount == -1 )
										LogLn<bright_blue>( "Targetting mob id %d", Globals::target_mob_type.back( ).first );
								}
							}
							else
							{
								auto mob_id = Helper::GetMobIDForName( tok );
								if ( mob_id == -1 )
								{
									LogLn<bright_red>( "%s invalid mob name.", tok.c_str( ) );
									return;
								}

								Globals::target_mob_type.push_back( { mob_id, kill_amount } );
								if ( kill_amount == -1 )
									LogLn<bright_blue>( "Targetting mob id %d", Globals::target_mob_type.back( ).first );
								else
									LogLn<bright_blue>( "Targetting mob id %d %d times", Globals::target_mob_type.back( ).first, Globals::target_mob_type.back( ).second );
							}
						}
					}
					else if ( ch == '*' )
					{
						Globals::target_mob_type.clear( );
						Globals::target_mob_type.push_back( { 0xCAFEBABE, -1 } );
						LogLn<bright_blue>( "!!Targetting All Mobs!!" );
					}
					else
					{
						if ( tokens.size( ) < 2 )
						{
							LogLn<bright_red>( "Invalid syntax. Either attack <names>,<of>,<mobs>.\nOr attack lower_level_diff,higher_level_diff" );
							return;
						}

						Globals::target_mob_type.clear( );

						// doing levels
						auto low = atol( tokens[ 0 ].c_str( ) );
						auto hi = atol( tokens[ 1 ].c_str( ) );
						Globals::target_mob_levels = { low,hi };
						LogLn<bright_blue>( "Targetting mobs between %d -> %d levels of the player", low, hi );
						Globals::exhaust_pause = false;
					}

					Globals::farming_target = nullptr;
				}
				else if ( strstr( input.c_str( ), "focus" ) )
				{
					if ( tokens.size( ) < 1 )
					{
						LogLn<bright_red>( "Invalid syntax. Proper use: focus <item_name>:optional_inv_count,<here>" );
						return;
					}

					Globals::ignore_items = false;
					Globals::desired_items.clear( );

					for ( unsigned int i = 0; i < tokens.size( ); i++ )
					{
						auto & tok = tokens[ i ];

						std::istringstream amt( tok );
						std::string amount;
						std::getline( amt, amount, ':' );
						std::getline( amt, amount, ':' ); // wouldbe the second one.
						int grab_amount = atol( amount.c_str( ) );
						if ( grab_amount == 0 )
							grab_amount = -1;

						auto pos = tok.find( ':' );
						if ( pos != tok.npos )
							tok = tok.substr( 0, pos );


						auto item_id = Helper::GetItemIDForName( tok );
						if ( item_id == -1 )
						{
							LogLn<bright_red>( "%s invalid item name.", tok.c_str( ) );
							return;
						}
						Globals::desired_items.push_back( { item_id, grab_amount } );
						if ( grab_amount == -1 )
							LogLn<bright_blue>( "Focusing item id %d", Globals::desired_items.back( ) );
						else
							LogLn<bright_blue>( "Focusing item id %d for %d times", Globals::desired_items.back( ), grab_amount );
					}
				}
				else if ( strstr( input.c_str( ), "ignore" ) )
				{
					if ( tokens.size( ) < 1 )
					{
						LogLn<bright_red>( "Invalid syntax. Proper use: ignore <item_name>,<here>" );
						return;
					}

					Globals::desired_items.clear( );
					Globals::ignore_items = true;

					if ( tokens[ 0 ].front( ) == '*' )
					{
						Globals::desired_items.push_back( { 0xCAFEBABE, -1 } );
						LogLn<bright_blue>( "Ignoring every item." );
						return;
					}

					for ( unsigned int i = 0; i < tokens.size( ); i++ )
					{
						auto id = Helper::GetItemIDForName( tokens[ i ] );
						if ( id == -1 )
						{
							LogLn<bright_red>( "Couldnt find item %s", tokens[ i ].c_str( ) );
							continue;
						}
						Globals::desired_items.push_back( { id , -1 } );
						LogLn<bright_blue>( "Ignoring item id %d", Globals::desired_items.back( ) );
					}
				}
				else if ( strstr( input.c_str( ), "query" ) )
				{
					if ( tokens.size( ) < 1 )
					{
						LogLn<bright_red>( "Invalid syntax. Proper use: query <item_name>,<here>" );
						return;
					}

					auto pLocal = Helper::GetLocalPlayer( );
					if ( !pLocal )
					{
						LogLn<bright_red>( "Local player doesnt exist!" );
						return;
					}

					for ( unsigned int i = 0; i < tokens.size( ); i++ )
					{
						auto item_id = Helper::GetItemIDForName( tokens[ i ].c_str( ) );
						if ( item_id == -1 )
						{
							LogLn<bright_red>( "Item %s doesnt exist!", tokens[ i ].c_str( ) );
						}
						else
						{
							auto item = Helper::GetInventoryItem( pLocal, item_id );
							LogLn<bright_blue>( "Amount of %s is %d", tokens[ i ].c_str( ), item->quantity );
						}
					}
				}
				else
					LogLn<bright_red>( "Invalid input %s. Type help for more information.", input.c_str( ) );
			}
		}
	}
}