#include "wings_globals.hpp"
#include "wings_helper.hpp"

namespace Input
{
	bool IsOnGround( );

	bool HasMovementState( PlayerInput::Movement flag );

	void SetMovementState( PlayerInput::Movement flag );

	void SetRotationFlag( PlayerInput::Rotation flag );
	void SetTranslationState( PlayerInput::Translation state );
}